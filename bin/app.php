#!/usr/bin/env php
<?php

use FlyingAnvil\SmuuUniverse\Cli\Command\UpdateStatisticsCommand;
use FlyingAnvil\SmuuUniverse\ContainerFactory;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\CommandLoader\ContainerCommandLoader;

require_once __DIR__ . '/../vendor/autoload.php';

(static function () {
    $root = dirname(__DIR__);
    define('PUBLIC_ROOT', $root . '/public');
    chdir($root);

    $container = ContainerFactory::create();

    $commandLoader = new ContainerCommandLoader($container, [
        UpdateStatisticsCommand::COMMAND_NAME => UpdateStatisticsCommand::class,
    ]);

    $app = new Application();
    $app->setCommandLoader($commandLoader);
    $app->setCatchExceptions(true);

    $app->run();
})();

