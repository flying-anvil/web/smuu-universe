<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\User;

use FlyingAnvil\SmuuUniverse\Api\User\DataObject\UserSelfResponse;
use FlyingAnvil\SmuuUniverse\Slim\AbstractAction;
use FlyingAnvil\SmuuUniverse\User\DataObject\User;
use FlyingAnvil\SmuuUniverse\User\Repository\BadgeRepository;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class UserGetSelfAction extends AbstractAction
{
    public function __construct(
        private BadgeRepository $badgeRepository,
    ) {}

    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        /** @var User $user */
        $user = $request->getAttribute('user');
        $badges = $this->badgeRepository->loadApiBadgesForUser($user->getId());

        $responseUser = UserSelfResponse::create($user, $badges);

        return $response->withJson($responseUser);
    }
}
