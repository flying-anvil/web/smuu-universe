<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\User;

use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\SmuuUniverse\Slim\AbstractAction;
use FlyingAnvil\SmuuUniverse\User\DataObject\Collection\Usernames;
use FlyingAnvil\SmuuUniverse\User\DataObject\Username;
use FlyingAnvil\SmuuUniverse\User\Repository\UserRepository;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class UserGetByNameAction extends AbstractAction
{
    public function __construct(
        private UserRepository $userRepository,
    ) {}

    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        $rawUserNames = $request->getQueryParam('name');

        if (is_string($rawUserNames)) {
            $rawUserNames = [$rawUserNames];
        }

        $names  = $this->rawNamesToNames($rawUserNames);
        $result = $this->userRepository->loadApiUsersByNames($names);

        return $response->withJson([
            'users' => $result,
        ]);
    }

    private function rawNamesToNames(array $rawNames): Usernames
    {
        $userNames = [];
        foreach ($rawNames as $rawName) {
            try {
                $userNames[] = Username::create($rawName);
            } catch (ValueException) {}
        }

        return Usernames::create(...$userNames);
    }
}
