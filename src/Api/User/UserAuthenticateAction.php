<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\User;

use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\SmuuUniverse\Exception\DuplicateEntryException;
use FlyingAnvil\SmuuUniverse\Slim\AbstractAction;
use FlyingAnvil\SmuuUniverse\User\DataObject\AuthKey;
use FlyingAnvil\SmuuUniverse\User\DataObject\Username;
use FlyingAnvil\SmuuUniverse\User\Repository\UserAuthKeyRepository;
use FlyingAnvil\SmuuUniverse\User\Repository\UserRepository;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class UserAuthenticateAction extends AbstractAction
{
    public function __construct(
        private UserRepository $userRepository,
        private UserAuthKeyRepository $authKeyRepository,
    ) {}

    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        $body = $request->getParsedBody();

        $rawUserName    = $body['name'];
        $rawPassword    = $body['password'];
        $rawDescription = $body['description'] ?? '';
        $rawExpire      = $body['expire'] ?? '7 days';

        try {
            $userName = Username::create($rawUserName);
            $expire = UtcDate::parse($rawExpire);
        } catch (ValueException $exception) {
            return $this->formatError($response, $exception->getMessage());
        }

//        $passwordValid = $this->userRepository->verifyUserPasswordByName($rawPassword, $userName);
        $user = $this->userRepository->loadUserByName($userName);

        if (!$user->getPassword()->isValid($rawPassword)) {
            return $this->formatError($response, 'Password is incorrect');
        }

        $authKey = AuthKey::generate($rawDescription, $expire);
        $this->authKeyRepository->checkExpiration($authKey, $user);

        try {
            $this->authKeyRepository->insertNewAuthKey($authKey, $user);
        } catch (DuplicateEntryException) {
            return $this->formatError($response, 'Session with this description already exists');
        }

        return $response->withJson([
            'authKey' => $authKey->getKey(),
            'expire'  => $expire->format(UtcDate::FORMAT_ATOM),
        ]);
    }
}
