<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\User;

use FlyingAnvil\SmuuUniverse\Slim\AbstractAction;
use FlyingAnvil\SmuuUniverse\User\DataObject\User;
use FlyingAnvil\SmuuUniverse\User\Repository\UserAuthKeyRepository;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class UserExpireAuthenticationAction extends AbstractAction
{
    public function __construct(
        private UserAuthKeyRepository $authKeyRepository,
    ) {}

    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        /** @var User $user */
        $user = $request->getAttribute('user');

        $body = $request->getParsedBody();
        $rawDescription = $body['description'] ?? null;

        if ($rawDescription === null) {
            return $this->formatError($response, 'Missing description/name');
        }

        $successfullyExpired = $this->authKeyRepository->expireAuthKeyByName($user, $rawDescription);

        if (!$successfullyExpired) {
            return $this->formatMessage($response, 'No such session', [], self::STATUS_GONE);
        }

        return $this->formatMessage($response, 'Successfully expired session');
    }
}
