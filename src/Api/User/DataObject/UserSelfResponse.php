<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\User\DataObject;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\SmuuUniverse\User\DataObject\Collection\ApiBadges;
use FlyingAnvil\SmuuUniverse\User\DataObject\User;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class UserSelfResponse implements DataObject
{
    private function __construct(
        private User $user,
        private ApiBadges $badges,
    ) {}

    public static function create(User $user, ApiBadges $badges): self
    {
        return new self($user, $badges);
    }

    public function jsonSerialize(): array
    {
        return [
            'name'        => $this->user->getName(),
            'dateJoined'  => $this->user->getDateJoined(),
            'badges'      => $this->badges,
            'permissions' => $this->user->getPermissions()->getFrontendPermissions()->toStringArray(),
        ];
    }
}
