<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\User;

use FlyingAnvil\Libfa\DataObject\Credentials\Password;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\SmuuUniverse\Exception\DuplicateEntryException;
use FlyingAnvil\SmuuUniverse\Options\PasswordOptions;
use FlyingAnvil\SmuuUniverse\Slim\AbstractAction;
use FlyingAnvil\SmuuUniverse\User\DataObject\Collection\Permissions;
use FlyingAnvil\SmuuUniverse\User\DataObject\Permission;
use FlyingAnvil\SmuuUniverse\User\DataObject\User;
use FlyingAnvil\SmuuUniverse\User\DataObject\Username;
use FlyingAnvil\SmuuUniverse\User\Repository\UserRepository;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class UserCreateAction extends AbstractAction
{
    public function __construct(
        private PasswordOptions $passwordOptions,
        private UserRepository $userRepository,
    ) {}

    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        $data = $request->getParsedBody();

        $rawName     = $data['name'];
        $rawPassword = $data['password'];

        $password = Password::createFromRaw(
            $rawPassword,
            PASSWORD_BCRYPT,
            $this->passwordOptions->getPepper(),
        );

        $user = User::create(
            SmallId::generate(),
            Username::create($rawName),
            UtcDate::now(),
            $password,
            Permissions::create(),
        );

        try {
            $this->userRepository->insertNewUser($user);
        } catch (DuplicateEntryException $exception) {
            return $this->formatError($response, $exception->getMessage(), self::STATUS_CONFLICT);
        }

        return $response->withStatus(self::STATUS_CREATED)
            ->withJson([
            'name' => $rawName,
        ]);
    }
}
