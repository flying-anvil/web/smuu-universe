<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\Misc;

use FlyingAnvil\Libfa\DataObject\Id\ID;
use FlyingAnvil\Libfa\DataObject\Id\LongId;
use FlyingAnvil\Libfa\DataObject\Id\MediumId;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\SmuuUniverse\Slim\AbstractAction;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class GenerateIdAction extends AbstractAction
{
    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        $amount = (int)$request->getQueryParam('amount', 1);
        $size   = $request->getQueryParam('size', 'small');

        $ids = [];
        for ($i = 0; $i < $amount; $i++) {
            $ids[] = $this->generateId($size);
        }

        return $response->withJson([
            'amount' => $amount,
            'size'   => $size,
            'ids'    => $ids,
        ]);
    }

    private function generateId(string $size): ID
    {
        return match ($size) {
            'medium' => MediumId::generate(),
            'long'   => LongId::generate(),

            default  => SmallId::generate(),
        };

    }
}
