<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\Status;

use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\SmuuUniverse\Slim\AbstractAction;
use FlyingAnvil\SmuuUniverse\Statistic\DataObject\HistoryStatistics;
use FlyingAnvil\SmuuUniverse\Statistic\Repository\StatisticsRepository;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class HistoryStatisticsAction extends AbstractAction
{
    public function __construct(
        private StatisticsRepository $statisticsRepository
    ) {}

    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        // Today plus 27 days = 28 days total
        $from = UtcDate::parse('now - 27 days');
        $to   = UtcDate::now();

        $history = $this->statisticsRepository->loadHistoryStatistics($from, $to);

        return $response->withJson([
            'history' => $history,
        ]);
    }
}
