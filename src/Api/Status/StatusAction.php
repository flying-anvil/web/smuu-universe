<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\Status;

use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\SmuuUniverse\Slim\AbstractAction;
use FlyingAnvil\SmuuUniverse\Statistic\Repository\StatisticsRepository;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class StatusAction extends AbstractAction
{
    public function __construct(
        private StatisticsRepository $statisticsRepository,
    ) {}

    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        $rawDay     = $request->getQueryParam('day', date('Y-m-d'));
        $day        = UtcDate::parse($rawDay);
        $statistics = $this->statisticsRepository->loadLatestStatistics();

        return $response->withJson([
            'health'     => 'healthy',
            'date'       => UtcDate::now(),
            'statistics' => $statistics,
        ]);
    }
}
