<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\Content\Level;

use FlyingAnvil\Libfa\DataObject\Id\LongId;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\SmuuUniverse\Content\DataObject\Level;
use FlyingAnvil\SmuuUniverse\Content\Repository\LevelRepository;
use FlyingAnvil\SmuuUniverse\Exception\DuplicateEntryException;
use FlyingAnvil\SmuuUniverse\Input\Processor\LevelCreateInputProcessor;
use FlyingAnvil\SmuuUniverse\Slim\AbstractAction;
use FlyingAnvil\SmuuUniverse\User\DataObject\User;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class LevelCreateAction extends AbstractAction
{
    public function __construct(
        private LevelCreateInputProcessor $inputProcessor,
        private LevelRepository $levelRepository,
    ) {}

    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        $body = $request->getParsedBody();

        $data = $this->inputProcessor->process($body);
        if ($data->hasErrors()) {
            return $this->formatError(
                $response,
                'Input is invalid',
                additionalData: ['errors' => $data->getErrors()],
            );
        }

        /** @var User $user */
        $user = $request->getAttribute('user');

        try {
            $this->levelRepository->insertNewLevel(Level::createStandalone(
                LongId::generate(),
                $data->getName(),
                $data->getDifficulty()->getId(),
                $user->getId(),
                UtcDate::now(),
            ), $user);
        } catch (DuplicateEntryException $exception) {
            return $this->formatError(
                $response,
                'Input is invalid',
                additionalData: ['errors' => ['name' => $exception->getMessage()]],
            );
        }

        return $response->withJson([
            'message' => 'Level successfully created',
        ]);
    }
}
