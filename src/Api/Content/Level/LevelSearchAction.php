<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\Content\Level;

use FlyingAnvil\SmuuUniverse\Content\Repository\LevelRepository;
use FlyingAnvil\SmuuUniverse\Input\Processor\LevelSearchInputProcessor;
use FlyingAnvil\SmuuUniverse\Slim\AbstractAction;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class LevelSearchAction extends AbstractAction
{
    public function __construct(
        private LevelSearchInputProcessor $inputProcessor,
        private LevelRepository $levelRepository,
    ) {}

    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        $data = $this->inputProcessor->process($request->getQueryParams());
        if ($data->hasErrors()) {
            return $this->formatError(
                $response,
                'Input is invalid',
                additionalData: ['errors' => $data->getErrors()],
            );
        }

         $levels = $this->levelRepository->searchApiLevels($data->getLevelSearch());

        return $response->withJson([
            'query'  => $request->getQueryParams(),
            'levels' => $levels,
        ]);
    }
}
