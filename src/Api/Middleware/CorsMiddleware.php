<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\Middleware;

use FlyingAnvil\Libfa\DataObject\Application\AppEnv;
use Slim\Http\Response;
use Slim\Http\ServerRequest;
use Slim\Routing\RouteContext;
use Slim\Routing\RouteRunner;

class CorsMiddleware
{
    public function __construct(
        private AppEnv $appEnv,
    ) {}

    public function __invoke(ServerRequest $request, RouteRunner $handler): Response
    {
        $routeContext   = RouteContext::fromRequest($request);
        $routingResults = $routeContext->getRoutingResults();
        $methods        = $routingResults->getAllowedMethods();
        $requestHeaders = $request->getHeaderLine('Access-Control-Request-Headers');

        /** @var Response $response */
        $response = $handler->handle($request);

        $allowedHost = $this->appEnv->is(AppEnv::ENV_TESTING)
            ? '*'
            : 'todo'; // TODO

        $response = $response
            ->withHeader('Access-Control-Allow-Origin', $allowedHost)
            ->withHeader('Access-Control-Allow-Methods', implode(',', $methods))
            ->withHeader('Access-Control-Allow-Headers', $requestHeaders)
            ->withHeader('Access-Control-Allow-Credentials', 'true');
            // Optional: Allow Ajax CORS requests with Authorization header

        return $response;
    }
}
