<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\Middleware;

use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\Libfa\DataObject\Id\EnormousId;
use FlyingAnvil\SmuuUniverse\Slim\AbstractMiddleware;
use FlyingAnvil\SmuuUniverse\User\Repository\UserRepository;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;
use Psr\Http\Message\ResponseInterface;

class AuthenticatedMiddleware extends AbstractMiddleware
{
    public function __construct(
        private UserRepository $userRepository,
    ) {}

    public function __invoke(ServerRequest $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $rawAuthKey = $request->getHeader('Auth-Key')[0] ?? null;

        if ($rawAuthKey === null) {
            return $this->formatError(
                $this->createNewReponse(),
                'Missing authentication',
                self::STATUS_UNAUTHORIZED,
            );
        }

        try {
            $authKeyId = EnormousId::createFromString($rawAuthKey);
        } catch (ValueException) {
            return $this->formatError(
                $this->createNewReponse(),
                'Authentication invalid',
                self::STATUS_UNAUTHORIZED,
            );
        }

        $user = $this->userRepository->loadUserByAuthKeyId($authKeyId);

        $authenticated = $user !== null;
        if (!$authenticated) {
            return $this->formatError(
                $this->createNewReponse(),
                'Authentication invalid',
                self::STATUS_UNAUTHORIZED,
            );
        }

        /** @var Response $response */
        $response = $handler->handle($request->withAttribute('user', $user));
        return $response;
    }
}
