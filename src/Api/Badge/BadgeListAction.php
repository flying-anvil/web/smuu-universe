<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\Badge;

use FlyingAnvil\SmuuUniverse\Slim\AbstractAction;
use FlyingAnvil\SmuuUniverse\User\Repository\BadgeRepository;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class BadgeListAction extends AbstractAction
{
    public function __construct(
        private BadgeRepository $badgeRepository,
    ) {}

    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        $badges = $this->badgeRepository->loadAllApiBadges();
        return $response->withJson([
            'badges' => $badges,
        ]);
    }
}
