<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api;

interface JsonResponseInterface
{
    public function getResponse(): array;
}
