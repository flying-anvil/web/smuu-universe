<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\Tip;

use FlyingAnvil\SmuuUniverse\Api\Tip\DataObject\TipResponse;
use FlyingAnvil\SmuuUniverse\Api\Tip\DataObject\TipsResponse;
use FlyingAnvil\SmuuUniverse\Slim\AbstractAction;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\Collection\Tips;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\Tip;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\TipTag;
use FlyingAnvil\SmuuUniverse\Tip\Repository\TipRepository;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class TipRandomAction extends AbstractAction
{
    public function __construct(
        private TipRepository $tipRepository,
    ) {}

    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        $rawTag = $request->getQueryParam('tag');
        $tip    = $this->getTip($rawTag);

        return $response->withJson(TipResponse::create($tip)->getResponse());
    }

    private function getTip(?string $rawTag): Tip
    {
        if ($rawTag !== null) {
            return $this->tipRepository->loadRandomTipByTagName($rawTag);
        }

        return $this->tipRepository->loadRandomTip();
    }
}
