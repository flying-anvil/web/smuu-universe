<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\Tip;

use FlyingAnvil\SmuuUniverse\Api\Tip\DataObject\TipsResponse;
use FlyingAnvil\SmuuUniverse\DataObject\Image;
use FlyingAnvil\SmuuUniverse\Slim\AbstractAction;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\Collection\Tips;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\TipTag;
use FlyingAnvil\SmuuUniverse\Tip\Repository\TipRepository;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class TipListAction extends AbstractAction
{
    public function __construct(
        private TipRepository $tipRepository,
    ) {}

    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        $rawTag = $request->getQueryParam('tag');
        $tips   = $this->getTips($rawTag);

//        return $response->withJson(($tips));
        return $response->withJson(TipsResponse::create($tips)->getResponse());
    }

    private function getTips(?string $rawTag): Tips
    {
        if ($rawTag !== null) {
            return $this->tipRepository->loadTipsByTagName($rawTag);
        }

        return $this->tipRepository->loadTips();
    }
}
