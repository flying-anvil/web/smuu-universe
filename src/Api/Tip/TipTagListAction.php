<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\Tip;

use FlyingAnvil\SmuuUniverse\Api\Tip\DataObject\TagsResponse;
use FlyingAnvil\SmuuUniverse\Slim\AbstractAction;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\Collection\Tips;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\TipTag;
use FlyingAnvil\SmuuUniverse\Tip\Repository\TipRepository;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class TipTagListAction extends AbstractAction
{
    public function __construct(
        private TipRepository $tipRepository,
    ) {}

    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        $tags = $this->tipRepository->loadTags();
        return $response->withJson(TagsResponse::create($tags)->getResponse());
    }
}
