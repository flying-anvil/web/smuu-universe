<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\Tip;

use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\SmuuUniverse\Slim\AbstractAction;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\Collection\TipTags;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\Tip;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\TipTag;
use FlyingAnvil\SmuuUniverse\Tip\Repository\TipRepository;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class TipCreateAction extends AbstractAction
{
    public function __construct(
        private TipRepository $tipRepository,
    ) {}

    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        $data = $request->getParsedBody();

        $tipText = $data['tip'];
        $rawTags = $data['tags'];

        $tags = [];

        foreach ($rawTags as $rawTag) {
            $tags[] = TipTag::create(
                SmallId::createFromString($rawTag),
                '',
            );
        }

        $tip = Tip::create(
            SmallId::generate(),
            $tipText,
            TipTags::create(...$tags),
        );

        $this->tipRepository->insertNewTip($tip);

        return $this->formatMessage($response, 'Created new tip', [
            'tipId' => $tip->getId(),
        ]);
    }
}
