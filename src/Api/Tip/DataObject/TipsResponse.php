<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\Tip\DataObject;

use FlyingAnvil\SmuuUniverse\Api\JsonResponseInterface;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\Collection\Tips;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class TipsResponse implements JsonResponseInterface
{
    public function __construct(private Tips $tips) {}

    public static function create(Tips $tips): self
    {
        return new self($tips);
    }

    public function getResponse(): array
    {
        $response = [];

        foreach ($this->tips as $tip) {
            $response[] = TipResponse::create($tip)->getResponse();
        }

        return $response;
    }
}
