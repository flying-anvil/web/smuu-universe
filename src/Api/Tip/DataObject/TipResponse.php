<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\Tip\DataObject;

use FlyingAnvil\SmuuUniverse\Api\JsonResponseInterface;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\Tip;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class TipResponse implements JsonResponseInterface
{
    private function __construct(private Tip $tip) {}

    public static function create(Tip $tip): self
    {
        return new self($tip);
    }

    public function getResponse(): array
    {
        return [
            'id'   => $this->tip->getId(),
            'tip'  => $this->tip->getValue(),
            'tags' => TagsResponse::create($this->tip->getTipTags())->getResponse(),
        ];
    }
}
