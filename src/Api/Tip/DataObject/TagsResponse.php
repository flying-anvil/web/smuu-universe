<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\Tip\DataObject;

use FlyingAnvil\SmuuUniverse\Api\JsonResponseInterface;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\Collection\TipTags;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class TagsResponse implements JsonResponseInterface
{
    private function __construct(private TipTags $tags) {}

    public static function create(TipTags $tags): self
    {
        return new self($tags);
    }

    public function getResponse(): array
    {
        $response = [];

        foreach ($this->tags as $tag) {
            $response[] = TagResponse::create($tag)->getResponse();
        }

        return $response;
    }
}
