<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Api\Tip\DataObject;

use FlyingAnvil\SmuuUniverse\Api\JsonResponseInterface;
use FlyingAnvil\SmuuUniverse\DataObject\Image;
use FlyingAnvil\SmuuUniverse\DataObject\Url;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\TipTag;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class TagResponse implements JsonResponseInterface
{
    private function __construct(private TipTag $tag) {}

    public static function create(TipTag $tag): self
    {
        return new self($tag);
    }

    public function getResponse(): array
    {
        return [
            'id'    => $this->tag->getId(),
            'name'  => $this->tag->getTagName(),
            'image' => $this->getImage(),
        ];
    }

    private function getImage(): Image
    {
        $path = sprintf(
            '/images/tip/tags/%s.png',
            $this->tag->getId(),
        );

        if (file_exists(PUBLIC_ROOT . $path)) {
            return Image::create(
                Url::create($path),
                $this->tag->getTagName(),
            );
        }

        return Image::create(
            Url::create('/images/tip/tags/Missing.png'),
            'Missing',
            false,
        );
    }
}
