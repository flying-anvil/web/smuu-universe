<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Tip\Repository;

use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\Collection\Tips;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\Collection\TipTags;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\Tip;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\TipTag;
use PDO;
use PDOException;

class TipRepository
{
    public function __construct(
        private PDO $pdo,
    ) {}

    public function loadTips(): Tips
    {
        $sql = <<< SQL
            SELECT
                t.tip as tip, t.tip_id as tip_id
            FROM tips t
            ORDER BY t.id
        SQL;

        $statement = $this->pdo->query($sql);

        $tips = [];
        foreach ($statement as $row) {
            $tipId = SmallId::createFromString($row['tip_id']);
            $tags  = $this->loadTagsForTip($tipId);
            $tips[] = Tip::create(
                $tipId,
                $row['tip'],
                $tags,
            );
        }

        return Tips::create(...$tips);
    }

    public function loadTipsByTagName(string $tipTag): Tips
    {
        $sql = <<< SQL
            SELECT
                t.tip as tip, t.tip_id as tip_id
            FROM tips t
            JOIN tip_tags tt on t.tip_id = tt.tip_id
            JOIN tips_tags tt2 on tt.tip_tag_id = tt2.tip_tag_id
            WHERE tt2.tag_name = :tag
            ORDER BY t.id
            SQL;

        $statement = $this->pdo->prepare($sql);
        $statement->execute([
            'tag' => $tipTag,
        ]);

        $tips = [];
        foreach ($statement as $row) {
            $tipId = SmallId::createFromString($row['tip_id']);
            $tags  = $this->loadTagsForTip($tipId);
            $tips[] = Tip::create(
                $tipId,
                $row['tip'],
                $tags,
            );
        }

        return Tips::create(...$tips);
    }

    public function loadRandomTip(): Tip
    {
        // This Query uses an AI column to determine the count of all entries
        // and selects a random entry based on its AI id
        // Might wanna use >= / <= id in case an id is missing?
        // Or ditch the whole thing, query all and select a random with PHP
        $sql = <<< SQL
            SELECT
                t.tip as tip, t.tip_id as tip_id
            FROM tips t
            JOIN
                (SELECT FLOOR(1 + RAND() *
                    (SELECT MAX(id) FROM tips)
                ) AS id) AS r
             WHERE t.id = r.id
        SQL;

        $statement = $this->pdo->query($sql);
        $row       = $statement->fetch();

        $tipId = SmallId::createFromString($row['tip_id']);
        $tags  = $this->loadTagsForTip($tipId);

        return Tip::create(
            $tipId,
            $row['tip'],
            $tags,
        );
    }

    public function loadRandomTipByTagName(string $tipTag): Tip
    {
        $tips   = $this->loadTipsByTagName($tipTag)->toArray();
        $random = array_rand($tips);

        return $tips[$random];
    }

    public function loadTags(): TipTags
    {
        $sql = <<< SQL
            SELECT * FROM tips_tags
        SQL;

        $statement = $this->pdo->query($sql);

        $tags = [];
        foreach ($statement as $row) {
            $tags[] = TipTag::create(
                SmallId::createFromString($row['tip_tag_id']),
                $row['tag_name'],
            );
        }

        return TipTags::create(...$tags);
    }

    public function insertNewTip(Tip $tip): void
    {
        $sqlTip = <<< SQL
            INSERT INTO tips (tip_id, tip)
            VALUE (:tipId, :tip)
        SQL;

        $sqlTag = <<< SQL
            INSERT INTO tip_tags (tip_id, tip_tag_id)
            VALUES (%s)
        SQL;

        $this->pdo->beginTransaction();

        try {
            $statementTip = $this->pdo->prepare($sqlTip);
            $statementTip->execute([
                'tipId' => $tip->getId()->toString(),
                'tip'   => $tip->getValue(),
            ]);

            $tagValues     = [];
            $tagParameters = [];
            foreach ($tip->getTipTags() as $index => $tag) {
                $keyTip = 'tipId_' . $index;
                $keyTag = 'tagId_' . $index;
                $tagValues[] = sprintf(
                    ':%s, :%s',
                    $keyTip,
                    $keyTag,
                );

                $tagParameters[$keyTip] = $tip->getId()->toString();
                $tagParameters[$keyTag] = $tag->getId()->toString();
            }

            $sqlTag = sprintf(
                $sqlTag,
                implode('), (', $tagValues),
            );

            $statementTag = $this->pdo->prepare($sqlTag);
            $statementTag->execute($tagParameters);

            $this->pdo->commit();
        } catch (PDOException $exception) {
            $this->pdo->rollBack();

            throw $exception;
        }
    }

    private function loadTagsForTip(SmallId $tipId): TipTags
    {
        $sql = <<< SQL
            SELECT tts.tag_name, tts.tip_tag_id
            FROM tips_tags tts
            JOIN tip_tags tt on tts.tip_tag_id = tt.tip_tag_id
            WHERE tt.tip_id = :tipId
        SQL;

        $statement = $this->pdo->prepare($sql);
        $statement->execute([
            'tipId' => $tipId->toString(),
        ]);

        $tags = [];
        foreach ($statement as $row) {
            $tags[] = TipTag::create(
                SmallId::createFromString($row['tip_tag_id']),
                $row['tag_name'],
            );
        }

        return TipTags::create(...$tags);
    }
}
