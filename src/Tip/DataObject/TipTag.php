<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Tip\DataObject;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use JetBrains\PhpStorm\Immutable;
use Stringable;

#[Immutable]
final class TipTag implements DataObject, StringValue, Stringable
{
    public const MAX_LENGTH = 32;

    protected function __construct(
        private SmallId $id,
        private string  $tagName,
    ) {
        if (mb_strlen($tagName) > self::MAX_LENGTH) {
            throw new ValueException(sprintf(
                'Value must be no longer than %d characters, %d given',
                self::MAX_LENGTH,
                mb_strlen($tagName),
            ));
        }
    }

    public static function create(SmallId $id, string $value): self
    {
        return new self($id, $value);
    }

    public function getTagName(): string
    {
        return $this->tagName;
    }

    public function getId(): SmallId
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return $this->tagName;
    }

    public function toString(): string
    {
        return $this->tagName;
    }

    public function jsonSerialize(): array
    {
        return [
            'id'   => $this->id,
            'name' => $this->tagName,
        ];
    }
}
