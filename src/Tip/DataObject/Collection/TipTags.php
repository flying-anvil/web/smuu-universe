<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Tip\DataObject\Collection;

use Countable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\TipTag;
use Generator;
use IteratorAggregate;

final class TipTags implements DataObject, IteratorAggregate, Countable
{
    /** @var TipTag[] */
    private array $tipTags;

    private function __construct(array $tipTags)
    {
        $this->tipTags = $tipTags;
    }

    public static function create(TipTag ...$tipTags): self
    {
        return new self($tipTags);
    }

    public function jsonSerialize(): array
    {
        return $this->tipTags;
    }

    /**
     * @return Generator<TipTag> | TipTag[]
     */
    public function getIterator(): Generator
    {
        yield from $this->tipTags;
    }

    public function count(): int
    {
        return count($this->tipTags);
    }
}
