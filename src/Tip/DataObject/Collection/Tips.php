<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Tip\DataObject\Collection;

use Countable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\Tip;
use Generator;
use IteratorAggregate;

final class Tips implements DataObject, IteratorAggregate, Countable
{
    /** @var Tip[] */
    private array $tips;

    private function __construct(array $tips)
    {
        $this->tips = $tips;
    }

    public static function create(Tip ...$tips): self
    {
        return new self($tips);
    }

    public function jsonSerialize(): array
    {
        return $this->tips;
    }

    /**
     * @return Generator<Tip> | Tip[]
     */
    public function getIterator(): Generator
    {
        yield from $this->tips;
    }

    public function toArray(): array
    {
        return $this->tips;
    }

    public function count(): int
    {
        return count($this->tips);
    }
}
