<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Tip\DataObject;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\Collection\TipTags;
use JetBrains\PhpStorm\Immutable;
use Stringable;

#[Immutable]
final class Tip implements DataObject, StringValue, Stringable
{
    public const MAX_LENGTH = 500;

    protected function __construct(
        private SmallId $id,
        private string $value,
        private TipTags $tipTags,
    ) {
        if (mb_strlen($value) > self::MAX_LENGTH) {
            throw new ValueException(sprintf(
                'Value must be no longer than %d characters, %d given',
                self::MAX_LENGTH,
                mb_strlen($value),
            ));
        }
    }

    public static function create(SmallId $id, string $value, TipTags $tipTags): self
    {
        return new self($id, $value, $tipTags);
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getId(): SmallId
    {
        return $this->id;
    }

    public function getTipTags(): TipTags
    {
        return $this->tipTags;
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function toString(): string
    {
        return $this->value;
    }

    public function jsonSerialize(): array
    {
        return [
            'id'   => $this->id,
            'tip'  => $this->value,
            'tags' => $this->tipTags,
        ];
    }
}
