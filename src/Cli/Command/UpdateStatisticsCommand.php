<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Cli\Command;

use Exception;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\SmuuUniverse\Statistic\DataObject\Statistics;
use FlyingAnvil\SmuuUniverse\Statistic\Repository\StatisticsRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateStatisticsCommand extends Command
{
    public const COMMAND_NAME = 'statistics:update';

    private const OPTION_FORCE_DATE = 'force-date';

    private ?UtcDate $forcedDate = null;

    public function __construct(
        private StatisticsRepository $statisticsRepository,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription('Updates the statistics for the current date');

        $this->addOption(
            self::OPTION_FORCE_DATE,
            null,
            InputOption::VALUE_REQUIRED,
            'Forces the date for which the statistics are updated. Meant for updating at midnight.',
        );
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $rawForceDate = $input->getOption(self::OPTION_FORCE_DATE);

//        $this->forcedDate = $rawForceDate !== null
//            ? UtcDate::parse($rawForceDate)
//            : null;

        if ($rawForceDate) {
            $this->forcedDate = UtcDate::parse($rawForceDate);
            if ($this->forcedDate < UtcDate::parse('yesterday')) {
                throw new Exception('Cannot force a date older than yesterday. This is a security mechanism.');
            }

            if (!$this->forcedDate->isToday()) {
                throw new Exception('Cannot force a date in the future.');
            }
        }
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $data = Statistics::create(
            $this->forcedDate ?? UtcDate::now(),
            $this->statisticsRepository->loadCountUsers(),
            0,
            0,
            $this->statisticsRepository->loadCountBadges(),
            $this->statisticsRepository->loadCountBadgesAwarded(),
            $this->statisticsRepository->loadCountLevels(),
            $this->statisticsRepository->loadCountWorlds(),
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        );

//        $output->writeln(json_encode($data, JSON_PRETTY_PRINT));

        $this->statisticsRepository->updateStatistics($data);

        return self::SUCCESS;
    }
}
