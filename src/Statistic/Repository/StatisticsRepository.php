<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Statistic\Repository;

use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\SmuuUniverse\Statistic\DataObject\HistoryStatistics;
use FlyingAnvil\SmuuUniverse\Statistic\DataObject\Statistics;
use PDO;

class StatisticsRepository
{
    public function __construct(
        private PDO $pdo,
    ) {}

    public function loadLatestStatistics(): Statistics
    {
        $sql = <<< SQL
            SELECT
                   s.count_users,
                   s.count_views,
                   s.count_news,

                   s.count_badges,
                   s.count_badges_awarded,

                   s.count_levels,
                   s.count_worlds,

                   s.count_tutorials,
                   s.count_documentations,
                   s.count_guides,

                   s.count_graphics,
                   s.count_music,
                   s.count_samples,
                   s.count_sounds,
                   s.count_blocks,
                   s.count_sprites
            FROM statistics s
            ORDER BY date DESC
            LIMIT 1
        SQL;

        $statement = $this->pdo->query($sql);
        $row = $statement->fetch();

        if ($row === false) {
            return Statistics::createEmpty(UtcDate::now());
        }

        return Statistics::create(
            UtcDate::now(),
            (int)$row['count_users'],
            (int)$row['count_views'],
            (int)$row['count_news'],
            (int)$row['count_badges'],
            (int)$row['count_badges_awarded'],
            (int)$row['count_levels'],
            (int)$row['count_worlds'],
            (int)$row['count_tutorials'],
            (int)$row['count_documentations'],
            (int)$row['count_guides'],
            (int)$row['count_graphics'],
            (int)$row['count_music'],
            (int)$row['count_samples'],
            (int)$row['count_sounds'],
            (int)$row['count_blocks'],
            (int)$row['count_sprites'],
        );
    }

    public function loadHistoryStatistics(UtcDate $from, UtcDate $to): HistoryStatistics
    {
        $sql = <<< SQL
            SELECT
                   s.date,
                   s.count_users,
                   s.count_views,
                   s.count_news,

                   s.count_badges,
                   s.count_badges_awarded,

                   s.count_levels,
                   s.count_worlds,

                   s.count_tutorials,
                   s.count_documentations,
                   s.count_guides,

                   s.count_graphics,
                   s.count_music,
                   s.count_samples,
                   s.count_sounds,
                   s.count_blocks,
                   s.count_sprites
            FROM statistics s
            WHERE date >= :from AND date <= :to
            ORDER BY date DESC
        SQL;

        $statement = $this->pdo->prepare($sql);
        $statement->execute([
            'from' => $from->format(UtcDate::FORMAT_HUMAN),
            'to'   => $to->format(UtcDate::FORMAT_HUMAN),
        ]);

        $statistics = [];
        foreach ($statement as $row) {
            $statistics[$row['date']] = Statistics::create(
                UtcDate::parse($row['date']),
                (int)$row['count_users'],
                (int)$row['count_views'],
                (int)$row['count_news'],
                (int)$row['count_badges'],
                (int)$row['count_badges_awarded'],
                (int)$row['count_levels'],
                (int)$row['count_worlds'],
                (int)$row['count_tutorials'],
                (int)$row['count_documentations'],
                (int)$row['count_guides'],
                (int)$row['count_graphics'],
                (int)$row['count_music'],
                (int)$row['count_samples'],
                (int)$row['count_sounds'],
                (int)$row['count_blocks'],
                (int)$row['count_sprites'],
            );
        }

        return HistoryStatistics::createWithDates($from, $to, ...$statistics);
    }

    public function updateStatistics(Statistics $statistics): void
    {
        $sql = <<< SQL
            INSERT INTO statistics (
                date,
                count_users,
                count_views,
                count_news,

                count_badges,
                count_badges_awarded,

                count_levels,
                count_worlds,

                count_tutorials,
                count_documentations,
                count_guides,

                count_graphics,
                count_music,
                count_samples,
                count_sounds,
                count_blocks,
                count_sprites
            )
            VALUE (
                :day,
                :countUsers,
                :countViews,
                :countNews,

                :countBadges,
                :countBadgesAwarded,

                :countLevels,
                :countWorlds,

                :countTutorials,
                :countDocumentations,
                :countGuides,

                :countGraphics,
                :countMusic,
                :countSamples,
                :countSounds,
                :countBlocks,
                :countSprites
            ) AS new
            ON DUPLICATE KEY UPDATE
                count_users          = new.count_users,
                count_views          = new.count_views,
                count_news           = new.count_news,

                count_badges         = new.count_badges,
                count_badges_awarded = new.count_badges_awarded,

                count_levels         = new.count_levels,
                count_worlds         = new.count_worlds,

                count_tutorials      = new.count_tutorials,
                count_documentations = new.count_documentations,
                count_guides         = new.count_guides,

                count_graphics       = new.count_graphics,
                count_music          = new.count_music,
                count_samples        = new.count_samples,
                count_sounds         = new.count_sounds,
                count_blocks         = new.count_blocks,
                count_sprites        = new.count_sprites
        SQL;

        $statement = $this->pdo->prepare($sql);
        $statement->execute([
            'day'                 => $statistics->getDay()->format(UtcDate::FORMAT_HUMAN),
            'countUsers'          => $statistics->getCountUsers(),
            'countViews'          => $statistics->getCountViews(),
            'countNews'           => $statistics->getCountNews(),

            'countBadges'         => $statistics->getCountBadges(),
            'countBadgesAwarded'  => $statistics->getCountBadgesAwarded(),

            'countLevels'         => $statistics->getCountLevels(),
            'countWorlds'         => $statistics->getCountWorlds(),

            'countTutorials'      => $statistics->getCountTutorials(),
            'countDocumentations' => $statistics->getCountDocumentations(),
            'countGuides'         => $statistics->getCountGuides(),

            'countGraphics'       => $statistics->getCountGraphics(),
            'countMusic'          => $statistics->getCountMusic(),
            'countSamples'        => $statistics->getCountSamples(),
            'countSounds'         => $statistics->getCountSounds(),
            'countBlocks'         => $statistics->getCountBlocks(),
            'countSprites'        => $statistics->getCountSprites(),
        ]);
    }

    public function loadCountUsers(): int
    {
        $sql = <<< SQL
            SELECT COUNT(id) as count FROM users
        SQL;

        return (int)($this->pdo->query($sql)->fetch()['count']);
    }

    public function loadCountBadges(): int
    {
        $sql = <<< SQL
            SELECT COUNT(badge_id) as count FROM badges
        SQL;

        return (int)($this->pdo->query($sql)->fetch()['count']);
    }

    public function loadCountBadgesAwarded(): int
    {
        $sql = <<< SQL
            SELECT COUNT(badge_id) as count FROM user_badges
        SQL;

        return (int)($this->pdo->query($sql)->fetch()['count']);
    }

    public function loadCountLevels(): int
    {
        $sql = <<< SQL
            SELECT COUNT(id) as count FROM levels
        SQL;

        return (int)($this->pdo->query($sql)->fetch()['count']);
    }

    public function loadCountWorlds(): int
    {
        $sql = <<< SQL
            SELECT COUNT(id) as count FROM levels
        SQL;

        return (int)($this->pdo->query($sql)->fetch()['count']);
    }
}
