<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Statistic\DataObject;

use Countable;
use DateInterval;
use DatePeriod;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use Generator;
use IteratorAggregate;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class HistoryStatistics implements DataObject, Countable, IteratorAggregate
{
    private function __construct(private array $statistics)
    {}

    public static function createWithDates(UtcDate $from, UtcDate $to, Statistics ...$statistics): self
    {
        $intervalOneDay = new DateInterval('P1D');
        $period         = new DatePeriod($from->getDate(), $intervalOneDay, $to->getDate()->add($intervalOneDay));

        $padded = [];
        foreach ($period as $day) {
            $dayFormatted = $day->format('Y-m-d');
            $padded[] = $statistics[$dayFormatted] ?? Statistics::createEmpty(UtcDate::createFromDateTime($day));
        }

        return new self($padded);
    }

    public function jsonSerialize(): array
    {
        return $this->statistics;
    }

    public function count(): int
    {
        return count($this->statistics);
    }

    /**
     * @return Generator | Statistics[]
     */
    public function getIterator(): Generator
    {
        yield from $this->statistics;
    }
}
