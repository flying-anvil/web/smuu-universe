<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Statistic\DataObject;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class Statistics implements DataObject
{
    private function __construct(
        private UtcDate $day,
        private int     $countUsers,
        private int     $countViews,
        private int     $countNews,
        private int     $countBadges,
        private int     $countBadgesAwarded,
        private int     $countLevels,
        private int     $countWorlds,
        private int     $countTutorials,
        private int     $countDocumentations,
        private int     $countGuides,
        private int     $countGraphics,
        private int     $countMusic,
        private int     $countSamples,
        private int     $countSounds,
        private int     $countBlocks,
        private int     $countSprites,
    ) {
    }

    public static function create(
        UtcDate $day,
        int     $countUsers,
        int     $countViews,
        int     $countNews,
        int     $countBadges,
        int     $countBadgesAwarded,
        int     $countLevels,
        int     $countWorlds,
        int     $countTutorials,
        int     $countDocumentations,
        int     $countGuides,
        int     $countGraphics,
        int     $countMusic,
        int     $countSamples,
        int     $countSounds,
        int     $countBlocks,
        int     $countSprites,
    ): self {
        return new self(...func_get_args());
    }

    public static function createEmpty(UtcDate $day): self
    {
        return new self($day,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        );
    }

    public function getDay(): UtcDate
    {
        return $this->day;
    }

    public function getCountUsers(): int
    {
        return $this->countUsers;
    }

    public function getCountViews(): int
    {
        return $this->countViews;
    }

    public function getCountNews(): int
    {
        return $this->countNews;
    }

    public function getCountBadges(): int
    {
        return $this->countBadges;
    }

    public function getCountBadgesAwarded(): int
    {
        return $this->countBadgesAwarded;
    }

    public function getCountLevels(): int
    {
        return $this->countLevels;
    }

    public function getCountWorlds(): int
    {
        return $this->countWorlds;
    }

    public function getCountTutorials(): int
    {
        return $this->countTutorials;
    }

    public function getCountDocumentations(): int
    {
        return $this->countDocumentations;
    }

    public function getCountGuides(): int
    {
        return $this->countGuides;
    }

    public function getCountGraphics(): int
    {
        return $this->countGraphics;
    }

    public function getCountMusic(): int
    {
        return $this->countMusic;
    }

    public function getCountSamples(): int
    {
        return $this->countSamples;
    }

    public function getCountSounds(): int
    {
        return $this->countSounds;
    }

    public function getCountBlocks(): int
    {
        return $this->countBlocks;
    }

    public function getCountSprites(): int
    {
        return $this->countSprites;
    }

    public function jsonSerialize(): array
    {
        return [
            'day'                 => $this->day->format('Y-m-d'),
            'countUsers'          => $this->countUsers,
            'countViews'          => $this->countViews,
            'countNews'           => $this->countNews,
            'countBadges'         => $this->countBadges,
            'countBadgesAwarded'  => $this->countBadgesAwarded,
            'countLevels'         => $this->countLevels,
            'countWorlds'         => $this->countWorlds,
            'countTutorials'      => $this->countTutorials,
            'countDocumentations' => $this->countDocumentations,
            'countGuides'         => $this->countGuides,
            'countGraphics'       => $this->countGraphics,
            'countMusic'          => $this->countMusic,
            'countSamples'        => $this->countSamples,
            'countSounds'         => $this->countSounds,
            'countBlocks'         => $this->countBlocks,
            'countSprites'        => $this->countSprites,
        ];
    }
}
