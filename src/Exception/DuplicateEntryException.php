<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Exception;

class DuplicateEntryException extends SmuuUniverseException
{
}
