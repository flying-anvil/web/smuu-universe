<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Exception;

use FlyingAnvil\Libfa\Exception\LibfaException;

class SmuuUniverseException extends LibfaException
{
}
