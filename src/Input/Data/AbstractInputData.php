<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Data;

abstract class AbstractInputData
{
    protected function __construct(
        protected Errors $errors,
    ) {}

    public function hasErrors(): bool
    {
        return $this->errors->hasErrors();
    }

    public function getErrors(): Errors
    {
        return $this->errors;
    }
}
