<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Data;

use FlyingAnvil\SmuuUniverse\Content\DataObject\LevelSearch;

class LevelSearchData extends AbstractInputData
{
    private function __construct(
        private ?LevelSearch $levelSearch,
        Errors $errors,
    ) {
        parent::__construct($errors);
    }

    public static function create(LevelSearch $levelSearch): self
    {
        return new self($levelSearch, Errors::createEmpty());
    }

    public static function createFromErrors(Errors $errors): self
    {
        return new self(null, null, $errors);
    }

    public function getLevelSearch(): ?LevelSearch
    {
        return $this->levelSearch;
    }
}
