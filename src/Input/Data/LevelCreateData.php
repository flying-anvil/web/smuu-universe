<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Data;

use FlyingAnvil\SmuuUniverse\Content\DataObject\Difficulty;
use FlyingAnvil\SmuuUniverse\Content\DataObject\LevelName;

class LevelCreateData extends AbstractInputData
{
    private function __construct(
        private ?LevelName  $levelSearch,
        private ?Difficulty $difficulty,
        Errors              $errors,
    ) {
        parent::__construct($errors);
    }

    public static function create(LevelName $levelName, Difficulty $difficulty): self
    {
        return new self($levelName, $difficulty, Errors::createEmpty());
    }

    public static function createFromErrors(Errors $errors): self
    {
        return new self(null, null, $errors);
    }

    public function getDifficulty(): Difficulty
    {
        return $this->difficulty;
    }

    public function getName(): LevelName
    {
        return $this->levelSearch;
    }
}
