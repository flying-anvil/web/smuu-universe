<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Data;

use Countable;
use FlyingAnvil\Libfa\DataObject\DataObject;

class Errors implements DataObject
{
    private bool $hasErrors;

    private function __construct(
        private array $errors = [],
    ) {
        $this->hasErrors = !empty($this->errors);
    }

    public static function createEmpty(): self
    {
        return new self();
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function addError(string $name, string $message): void
    {
        $this->errors[$name] = $message;
        $this->hasErrors = true;
    }

    public function hasErrors(): bool
    {
        return $this->hasErrors;
    }

    public function jsonSerialize(): array
    {
        return $this->errors;
    }
}
