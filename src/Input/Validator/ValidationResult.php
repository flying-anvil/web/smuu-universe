<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Validator;

use FlyingAnvil\Libfa\DataObject\DataObject;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class ValidationResult implements DataObject
{
    private function __construct(
        private bool $isValid,
        private string $message,
        private mixed $value = null,
    ) {}

    public static function create(bool $isValid, string $message = ''): self
    {
        return new self($isValid, $message);
    }

    public static function createValid(mixed $value = null, string $message = ''): self
    {
        return new self(true, $message, $value);
    }

    public static function createInvalid(string $message): self
    {
        return new self(false, $message);
    }

    public function isValid(): bool
    {
        return $this->isValid;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function jsonSerialize(): array
    {
        return [
            'isValid' => $this->isValid,
            'message' => $this->message,
        ];
    }
}
