<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Validator;

interface InputValidatorInterface
{
    public function validate(mixed $data): ValidationResult;
}
