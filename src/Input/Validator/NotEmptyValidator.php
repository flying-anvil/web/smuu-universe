<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Validator;

class NotEmptyValidator implements InputValidatorInterface
{
    public function validate(mixed $data): ValidationResult
    {
        if ($data === '') {
            return ValidationResult::create(false, 'Data is empty');
        }

        return ValidationResult::create(true);
    }
}
