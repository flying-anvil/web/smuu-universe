<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Validator;

use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;

class SmallIdValidator implements InputValidatorInterface
{
    public function validate(mixed $data): ValidationResult
    {
        if (!is_string($data)) {
            return ValidationResult::createInvalid('Value must be a string');
        }

        try {
            SmallId::createFromString($data);
        } catch (ValueException $exception) {
            return ValidationResult::createInvalid($exception->getMessage());
        }

        return ValidationResult::createValid();
    }
}
