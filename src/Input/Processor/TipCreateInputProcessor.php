<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Processor;

use FlyingAnvil\SmuuUniverse\Input\Data\AbstractInputData;
use FlyingAnvil\SmuuUniverse\Input\Data\Errors;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\Collection\TipTags;
use FlyingAnvil\SmuuUniverse\Tip\DataObject\Tip;

class TipCreateInputProcessor // extends AbstractInputProcessor implements InputProcessorInterface
{
    public function process(array $data): ?AbstractInputData
    {
        $errors = Errors::createEmpty();

        if (!isset($data['tip'])) {
            $errors->addError('tip', 'Missing value');
        }

        if (!isset($data['tags'])) {
            $errors->addError('tags', 'Missing value');
        }

        if (!is_array($data['tags'])) {
            $errors->addError('tags', 'Must be an array');
        }

        if ($errors->hasErrors()) {
//            return null;
        }

        try {
            $tags = TipTags::create();
            $tip = Tip::create();
        } catch (\Exception) {}

        return null;
    }
}
