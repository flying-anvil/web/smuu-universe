<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Processor\FieldProcessor;

use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\SmuuUniverse\Content\DataObject\LevelName;
use FlyingAnvil\SmuuUniverse\Input\Data\Errors;
use FlyingAnvil\SmuuUniverse\Input\Exception\InputMissingException;
use FlyingAnvil\SmuuUniverse\Input\Exception\InputValidationException;

class DifficultyIdFieldProcessor implements FieldProcessorInterface
{
    private const FIELD = 'difficulty';

    public function process(mixed $data, Errors $errors): SmallId
    {
        if (!array_key_exists(self::FIELD, $data)) {
            $errors->addError(self::FIELD, 'Value is missing');
            throw new InputMissingException(self::FIELD);
        }

        try {
            return SmallId::createFromString($data[self::FIELD]);
        } catch (ValueException $exception) {
            $errors->addError(self::FIELD, $exception->getMessage());
        }

        throw new InputValidationException(self::FIELD);
    }
}
