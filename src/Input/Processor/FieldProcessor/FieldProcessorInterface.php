<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Processor\FieldProcessor;

use FlyingAnvil\SmuuUniverse\Input\Data\Errors;
use FlyingAnvil\SmuuUniverse\Input\Exception\InputValidationException;

interface FieldProcessorInterface
{
    /**
     * @throws InputValidationException
     */
    public function process(mixed $data, Errors $errors): mixed;
}
