<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Processor\FieldProcessor;

use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\SmuuUniverse\Content\DataObject\LevelName;
use FlyingAnvil\SmuuUniverse\Input\Data\Errors;
use FlyingAnvil\SmuuUniverse\Input\Exception\InputMissingException;
use FlyingAnvil\SmuuUniverse\Input\Exception\InputValidationException;

class PerPageFieldProcessor implements FieldProcessorInterface
{
    private const FIELD = 'perPage';

    public function process(mixed $data, Errors $errors, ?int $default = null): int
    {
        if (!array_key_exists(self::FIELD, $data) && $default === null) {
            $errors->addError(self::FIELD, 'Value is missing');
            throw new InputMissingException(self::FIELD);
        }

        try {
            return (int)($data[self::FIELD] ?? $default);
        } catch (ValueException $exception) {
            $errors->addError(self::FIELD, $exception->getMessage());
        }

        throw new InputValidationException(self::FIELD);
    }
}
