<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Processor\FieldProcessor;

use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\SmuuUniverse\Content\DataObject\Difficulty;
use FlyingAnvil\SmuuUniverse\Content\DataObject\LevelName;
use FlyingAnvil\SmuuUniverse\Content\Repository\DifficultyRepository;
use FlyingAnvil\SmuuUniverse\Input\Data\Errors;
use FlyingAnvil\SmuuUniverse\Input\Exception\InputValidationException;

class DifficultyByIdFieldProcessor implements FieldProcessorInterface
{
    private const FIELD = 'difficulty';

    public function __construct(
        private DifficultyRepository $difficultyRepository,
        private DifficultyIdFieldProcessor $difficultyProcessor,
    ) {}

    public function process(mixed $data, Errors $errors): Difficulty
    {
        $difficultyId = $this->difficultyProcessor->process($data, $errors);

        $difficulty = $this->difficultyRepository->loadDifficultyById($difficultyId);

        if ($difficulty === null) {
            $errors->addError(self::FIELD, 'Difficulty does not exist');
            throw new InputValidationException(self::FIELD);
        }

        return $difficulty;
    }
}
