<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Processor;

use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\SmuuUniverse\Content\DataObject\LevelSearch;
use FlyingAnvil\SmuuUniverse\Input\Data\Errors;
use FlyingAnvil\SmuuUniverse\Input\Data\LevelSearchData;
use FlyingAnvil\SmuuUniverse\Input\Exception\InputMissingException;
use FlyingAnvil\SmuuUniverse\Input\Exception\InputValidationException;
use FlyingAnvil\SmuuUniverse\Input\Processor\FieldProcessor\DifficultyByIdFieldProcessor;
use FlyingAnvil\SmuuUniverse\Input\Processor\FieldProcessor\FieldProcessorInterface;
use FlyingAnvil\SmuuUniverse\Input\Processor\FieldProcessor\LevelNameFieldProcessor;
use FlyingAnvil\SmuuUniverse\Input\Processor\FieldProcessor\PageFieldProcessor;
use FlyingAnvil\SmuuUniverse\Input\Processor\FieldProcessor\PerPageFieldProcessor;
use FlyingAnvil\SmuuUniverse\Input\Processor\FieldProcessor\UserNameFieldProcessor;

class LevelSearchInputProcessor implements InputProcessorInterface
{
    public function __construct(
        private PerPageFieldProcessor $perPageProcessor,
        private PageFieldProcessor $pageProcessor,
        private LevelNameFieldProcessor $levelNameProcessor,
        private DifficultyByIdFieldProcessor $difficultyProcessor,
        private UserNameFieldProcessor $usernameProcessor,
    ) {}

    public function process(array $data): LevelSearchData
    {
        $errors = Errors::createEmpty();

        try {
            $perPage    = $this->perPageProcessor->process($data, $errors, 10);
            $page       = $this->pageProcessor->process($data, $errors, 1);
            $levelName  = $this->getOptionalValue($this->levelNameProcessor, $data, $errors);
            $difficulty = $this->getOptionalValue($this->difficultyProcessor, $data, $errors);
            $username   = $this->getOptionalValue($this->usernameProcessor, $data, $errors);
        } catch (InputValidationException) {
            return LevelSearchData::createFromErrors($errors);
        }

        $dateFrom = null;
        $dateTo   = null;

        if (isset($data['dateFrom'])) {
            $dateFrom = UtcDate::parse($data['dateFrom']);
        }

        if (isset($data['dateTo'])) {
            $dateTo = UtcDate::parse($data['dateTo']);
        }

        return LevelSearchData::create(LevelSearch::create(
            $perPage,
            $page,
            $levelName,
            $difficulty?->getId(),
            $username,
            $dateFrom,
            $dateTo,
        ));
    }

    private function getOptionalValue(FieldProcessorInterface $processor, array $data, Errors $errors): mixed
    {
        try {
            return $processor->process($data, $errors);
        } catch (InputMissingException) {
            return null;
        }
    }
}
