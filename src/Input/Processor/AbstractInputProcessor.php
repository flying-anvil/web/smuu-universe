<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Processor;

use FlyingAnvil\SmuuUniverse\Input\Validator\InputValidatorInterface;

abstract class AbstractInputProcessor
{
    protected function validateMultipleFields(InputValidatorInterface $validator, array $fields, array $data): array
    {
        $errors = [];

        foreach ($fields as $field) {
            if (!array_key_exists($field, $data)) {
                $errors[$field] = 'Data is missing';
                continue;
            }

            $result = $validator->validate($data[$field]);

            if (!$result->isValid()) {
                $errors[$field] = $result->getMessage();
            }
        }

        return $errors;
    }
}
