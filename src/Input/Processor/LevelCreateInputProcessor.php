<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Processor;

use FlyingAnvil\SmuuUniverse\Input\Data\Errors;
use FlyingAnvil\SmuuUniverse\Input\Data\LevelCreateData;
use FlyingAnvil\SmuuUniverse\Input\Exception\InputValidationException;
use FlyingAnvil\SmuuUniverse\Input\Processor\FieldProcessor\DifficultyByIdFieldProcessor;
use FlyingAnvil\SmuuUniverse\Input\Processor\FieldProcessor\LevelNameFieldProcessor;

class LevelCreateInputProcessor extends AbstractInputProcessor
{
    public function __construct(
        private LevelNameFieldProcessor      $nameProcessor,
        private DifficultyByIdFieldProcessor $difficultyProcessor,
    ) {}

    public function process(array $data): LevelCreateData
    {
        $errors = Errors::createEmpty();

        try {
            $levelName = $this->nameProcessor->process($data, $errors);
            $difficulty = $this->difficultyProcessor->process($data, $errors);
        } catch (InputValidationException) {
            return LevelCreateData::createFromErrors($errors);
        }

        return LevelCreateData::create(
            $levelName,
            $difficulty,
        );
    }
}
