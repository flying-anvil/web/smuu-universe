<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Processor;

use FlyingAnvil\SmuuUniverse\Input\Data\AbstractInputData;

interface InputProcessorInterface
{
    public function process(array $data): AbstractInputData;
}
