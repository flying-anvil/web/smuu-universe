<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Exception;

use FlyingAnvil\SmuuUniverse\Exception\SmuuUniverseException;
use Throwable;

class InputValidationException extends SmuuUniverseException
{
    public function __construct(string $field, $code = 0, Throwable $previous = null)
    {
        $message = sprintf(
            'Field "%s" is invalid',
            $field,
        );

        parent::__construct($message, $code, $previous);
    }
}
