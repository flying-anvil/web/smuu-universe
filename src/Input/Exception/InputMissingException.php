<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Input\Exception;

use FlyingAnvil\SmuuUniverse\Exception\SmuuUniverseException;
use Throwable;

class InputMissingException extends InputValidationException
{
}
