<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Slim;

use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Factory\DecoratedResponseFactory;
use Slim\Http\Response;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Psr7\Factory\StreamFactory;

abstract class AbstractMiddleware implements MiddlewareInterface, StatusCodeInterface
{
    protected function createNewReponse(int $code = StatusCodeInterface::STATUS_OK): Response
    {
        return (new DecoratedResponseFactory(
            new ResponseFactory(),
            new StreamFactory(),
        ))->createResponse($code);
    }

    protected function formatMessage(
        Response $response,
        string $message,
        array $additionalData = [],
        int $statusCode = self::STATUS_OK,
    ): Response {
        return $response->withStatus($statusCode)
            ->withJson($additionalData + [
                'message' => $message,
            ], options: JSON_UNESCAPED_SLASHES);
    }

    protected function formatError(
        Response $response,
        string $message,
        int $statusCode = self::STATUS_BAD_REQUEST,
        array $additionalData = [],
    ): Response {
        return $response->withStatus($statusCode)
            ->withJson([
                'error' => $message,
            ] + $additionalData, options: JSON_UNESCAPED_SLASHES);
    }
}
