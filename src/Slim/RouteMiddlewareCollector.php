<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Slim;

use Fig\Http\Message\StatusCodeInterface;
use FlyingAnvil\Libfa\DataObject\Application\AppEnv;
use FlyingAnvil\SmuuUniverse\Api\Badge\BadgeListAction;
use FlyingAnvil\SmuuUniverse\Api\Badge\BadgeStatisticsAction;
use FlyingAnvil\SmuuUniverse\Api\Content\Level\LevelCreateAction;
use FlyingAnvil\SmuuUniverse\Api\Content\Level\LevelSearchAction;
use FlyingAnvil\SmuuUniverse\Api\Middleware\AuthenticatedMiddleware;
use FlyingAnvil\SmuuUniverse\Api\Middleware\CorsMiddleware;
use FlyingAnvil\SmuuUniverse\Api\Misc\GenerateIdAction;
use FlyingAnvil\SmuuUniverse\Api\Status\HistoryStatisticsAction;
use FlyingAnvil\SmuuUniverse\Api\Status\StatusAction;
use FlyingAnvil\SmuuUniverse\Api\Tip\TipCreateAction;
use FlyingAnvil\SmuuUniverse\Api\Tip\TipListAction;
use FlyingAnvil\SmuuUniverse\Api\Tip\TipRandomAction;
use FlyingAnvil\SmuuUniverse\Api\Tip\TipTagListAction;
use FlyingAnvil\SmuuUniverse\Api\User\UserAuthenticateAction;
use FlyingAnvil\SmuuUniverse\Api\User\UserCreateAction;
use FlyingAnvil\SmuuUniverse\Api\User\UserExpireAuthenticationAction;
use FlyingAnvil\SmuuUniverse\Api\User\UserGetByNameAction;
use FlyingAnvil\SmuuUniverse\Api\User\UserGetSelfAction;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Slim\App;
use Slim\Http\Response;
use Slim\Http\ServerRequest;
use Slim\Routing\RouteCollectorProxy;
use Throwable;

final class RouteMiddlewareCollector
{
    public function register(App $app, AppEnv $appEnv): void
    {
        $app->add(CorsMiddleware::class);

        $app->addRoutingMiddleware();

        $this->registerPreflight($app, $appEnv);
        $this->registerErrorMiddleware($app);
        $this->registerApiRoutes($app);
        $this->registerNotFoundRoutes($app);
    }

    public function registerPreflight(App $app, AppEnv $appEnv): void
    {
        $app->options('/{routes:.+}', function (ServerRequest $request, Response $response): Response {
            // Do nothing here. Just return the response.
            return $response;
        });
    }

    private function registerApiRoutes(App $app): void
    {
        $app->get('/', function (ServerRequestInterface $request, Response $response) {
            return $response->write('Nothing here, try the <a href="/api/status">API</a>')->withStatus(404);
        })->setName('index');

        $app->group('/api', function (RouteCollectorProxy $group) {
            $publicMiddlewares = [];
            $protectedMiddlewares = [AuthenticatedMiddleware::class];

            $routes = [
                [
                    'route'       => $group->get('/status', StatusAction::class)->setName('api-status'),
                    'middlewares' => $publicMiddlewares,
                ],
                [
                    'route'       => $group->get('/statistics/history', HistoryStatisticsAction::class)->setName('api-statistics-history'),
                    'middlewares' => $publicMiddlewares,
                ],
                [
                    'route'       => $group->get('/misc/id', GenerateIdAction::class)->setName('api-misc-id'),
                    'middlewares' => $publicMiddlewares,
                ],
                [
                    'route'       => $group->post('/user', UserCreateAction::class)->setName('api-user-create'),
                    'middlewares' => $publicMiddlewares,
                ],
                [
                    'route'       => $group->get('/user', UserGetByNameAction::class)->setName('api-user-get'),
                    'middlewares' => $publicMiddlewares,
                ],
                [
                    'route'       => $group->get('/user/self', UserGetSelfAction::class)->setName('api-user-get-self'),
                    'middlewares' => $protectedMiddlewares,
                ],
                [
                    'route'       => $group->post('/user/authenticate', UserAuthenticateAction::class)->setName('api-user-authenticate'),
                    'middlewares' => $publicMiddlewares,
                ],
                [
                    'route'       => $group->post('/user/authenticate/expire', UserExpireAuthenticationAction::class)->setName('api-user-authenticate-expire'),
                    'middlewares' => $protectedMiddlewares,
                ],
                [
                    'route'       => $group->get('/badge', BadgeListAction::class)->setName('api-badge-list'),
                    'middlewares' => $publicMiddlewares,
                ],
                [
                    'route'       => $group->get('/badge/statistics', BadgeStatisticsAction::class)->setName('api-badge-statistics'),
                    'middlewares' => $publicMiddlewares,
                ],
                [
                    'route'       => $group->post('/content/level', LevelCreateAction::class)->setName('api-content-level-create'),
                    'middlewares' => $protectedMiddlewares,
                ],
                [
                    'route'       => $group->get('/content/level/search', LevelSearchAction::class)->setName('api-content-level-create'),
                    'middlewares' => $publicMiddlewares,
                ],
                [
                    'route'       => $group->get('/tip', TipListAction::class)->setName('api-tip-list'),
                    'middlewares' => $publicMiddlewares,
                ],
                [
                    'route'       => $group->post('/tip', TipCreateAction::class)->setName('api-tip-create'),
                    'middlewares' => $protectedMiddlewares,
                ],
                [
                    'route'       => $group->get('/tip/random', TipRandomAction::class)->setName('api-tip-random'),
                    'middlewares' => $publicMiddlewares,
                ],
                [
                    'route'       => $group->get('/tip/tag', TipTagListAction::class)->setName('api-tip-tag-list'),
                    'middlewares' => $publicMiddlewares,
                ],
            ];

            foreach ($routes as $routeDefinition) {
                $route = $routeDefinition['route'];
                foreach ($routeDefinition['middlewares'] as $middleware) {
                    $route->add($middleware);
                }
            }
        });
    }

    public function registerNotFoundRoutes(App $app): void
    {
        $callback = function (ServerRequestInterface $request, Response $response) {
            $content404 = '404 - Not Found';
            return $response->withStatus(404)
                ->withHeader('Content-Type', 'text/html')
                ->write($content404);
        };

        $callbackApi = function (ServerRequestInterface $request, Response $response) {
            return $response->withStatus(404)
                ->withJson([
                    'message' => 'Not Found',
                ]);
        };

        $app->any('/api/{routes:.+}', $callbackApi);
        $app->any('/{routes:(?!api/).+}', $callback);
    }

    private function registerErrorMiddleware(App $app): void
    {
        $errorMiddleware = $app->addErrorMiddleware(true, true, true);
        $errorMiddleware->setDefaultErrorHandler(function (
            ServerRequestInterface $request,
            Throwable              $exception,
            bool                   $displayErrorDetails,
            bool                   $logErrors,
            bool                   $logErrorDetails,
            ?LoggerInterface       $logger = null
        ) use ($app): Response {
            $payload = [
                'type'     => $exception::class,
                'error'    => $exception->getMessage(),
                'occurred' => sprintf(
                    '%s:%d',
                    $exception->getFile(),
                    $exception->getLine(),
                ),
            ];

            $response = $app->getResponseFactory()->createResponse();

            /** @var Response $response */
            return $response
                ->withHeader('Access-Control-Allow-Origin', '*')
                ->withHeader('Access-Control-Allow-Credentials', 'true')
                ->withStatus(StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR)
                ->withJson($payload, options: JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        });
    }
}
