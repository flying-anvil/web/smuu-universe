<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Slim;

use Psr\Http\Message\ResponseInterface;
use Slim\Http\ServerRequest;

interface MiddlewareInterface
{
    public function __invoke(ServerRequest $request, \Psr\Http\Server\RequestHandlerInterface $handler): ResponseInterface;
}
