<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Slim;

use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

interface RequestHandlerInterface
{
    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface;
}
