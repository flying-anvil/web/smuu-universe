<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\DataObject;

use FlyingAnvil\Libfa\DataObject\DataObject;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class Image implements DataObject
{
    private function __construct(
        private Url $source,
        private string $name,
        private bool $exists,
    ) {}

    public static function create(Url $url, string $name, bool $exists = true): self
    {
        return new self($url, $name, $exists);
    }

    public function jsonSerialize(): array
    {
        return [
            'source' => $this->source,
            'name'   => $this->name,
            'exists' => $this->exists,
        ];
    }
}
