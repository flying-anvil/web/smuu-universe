<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\DataObject;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use Stringable;

abstract class AbstractStringDataObject implements DataObject, StringValue, Stringable
{
    protected function __construct(
        private string $value,
    ) {
        $maxLength = $this->getMaxLength();
        if (mb_strlen($value) > $maxLength) {
            throw new ValueException(sprintf(
                'Value must be no longer than %d characters, %d given',
                $maxLength,
                mb_strlen($value),
            ));
        }
    }

    abstract protected function getMaxLength(): int;

    public static function create(string $value): static
    {
        return new static($value);
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function toString(): string
    {
        return $this->value;
    }

    public function jsonSerialize(): string|array
    {
        return $this->value;
    }
}
