<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\DataObject;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use JetBrains\PhpStorm\Immutable;
use Stringable;

#[Immutable]
final class Url implements DataObject, StringValue, Stringable
{
    public function __construct(private string $url) {}

    public static function create(string $url): self
    {
        return new self($url);
    }

    public function __toString(): string
    {
        return $this->url;
    }

    public function toString(): string
    {
        return $this->url;
    }

    public function jsonSerialize(): string
    {
        return $this->url;
    }
}
