<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Content\DataObject;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use JetBrains\PhpStorm\Immutable;
use Stringable;

#[Immutable]
final class Difficulty implements DataObject, Stringable, StringValue
{
    private function __construct(
        private SmallId $id,
        private DifficultyName $name,
        private int $level,
        private int $score,
    ) {}

    public static function create(SmallId $id, DifficultyName $name, int $level, int $score): self {
        return new self($id, $name, $level, $score);
    }

    public function getId(): SmallId
    {
        return $this->id;
    }

    public function getName(): DifficultyName
    {
        return $this->name;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function getScore(): int
    {
        return $this->score;
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function toString(): string
    {
        return sprintf(
            '%s %d',
            $this->name,
            $this->level,
        );
    }

    public function jsonSerialize(): array
    {
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'level' => $this->level,
            'score' => $this->score,
        ];
    }
}
