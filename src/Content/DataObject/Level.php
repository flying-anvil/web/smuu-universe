<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Content\DataObject;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Id\LongId;
use FlyingAnvil\Libfa\DataObject\Id\MediumId;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class Level implements DataObject
{
    private function __construct(
        private LongId $id,
        private LevelName $name,
        private SmallId $difficultyId,
        private SmallId $userId,
        private ?MediumId $worldId,
        private UtcDate $dateCreated,
    ) {}

    public static function create(
        LongId $id,
        LevelName $name,
        SmallId $difficultyId,
        SmallId $userId,
        ?MediumId $worldId,
        UtcDate $dateCreated,
    ): self {
        return new self($id, $name, $difficultyId, $userId, $worldId, $dateCreated);
    }

    public static function createStandalone(
        LongId $id,
        LevelName $name,
        SmallId $difficultyId,
        SmallId $userId,
        UtcDate $dateCreated,
    ): self {
        return new self($id, $name, $difficultyId, $userId, null, $dateCreated);
    }

    public function getId(): LongId
    {
        return $this->id;
    }

    public function getName(): LevelName
    {
        return $this->name;
    }

    public function getDifficultyId(): SmallId
    {
        return $this->difficultyId;
    }

    public function getUserId(): SmallId
    {
        return $this->userId;
    }

    public function getWorldId(): ?MediumId
    {
        return $this->worldId;
    }

    public function getDateCreated(): UtcDate
    {
        return $this->dateCreated;
    }

    public function jsonSerialize(): array
    {
        return [
            'id'           => $this->id,
            'name'         => $this->name,
            'difficultyId' => $this->difficultyId,
            'userId'       => $this->userId,
            'worldId'      => $this->worldId,
            'dateCreated'  => $this->dateCreated,
        ];
    }
}
