<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Content\DataObject;

use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\SmuuUniverse\User\DataObject\Username;

class LevelSearch
{
    private function __construct(
        private int $perPage,
        private int $page,
        private ?LevelName $levelName,
        private ?SmallId $difficultyId,
        private ?Username $username,
        private ?UtcDate $dateFrom,
        private ?UtcDate $dateTo,
    ) {}

    public static function create(
        int $perPage,
        int $page,
        ?LevelName $levelName,
        ?SmallId $difficultyId,
        ?Username $username,
        ?UtcDate $dateFrom,
        ?UtcDate $dateTo,
    ): self {
        return new self($perPage, $page, $levelName, $difficultyId, $username, $dateFrom, $dateTo);
    }

    public function getPerPage(): int
    {
        return $this->perPage;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getLevelName(): ?LevelName
    {
        return $this->levelName;
    }

    public function getDifficultyId(): ?SmallId
    {
        return $this->difficultyId;
    }

    public function getUsername(): ?Username
    {
        return $this->username;
    }

    public function getDateFrom(): ?UtcDate
    {
        return $this->dateFrom;
    }

    public function getDateTo(): ?UtcDate
    {
        return $this->dateTo;
    }
}
