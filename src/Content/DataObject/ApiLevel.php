<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Content\DataObject;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\SmuuUniverse\User\DataObject\Username;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class ApiLevel implements DataObject
{
    private function __construct(
        private LevelName  $name,
        private Difficulty $difficulty,
        private Username   $username,
        private UtcDate    $dateCreated,
    ) {
    }

    public static function create(
        LevelName  $name,
        Difficulty $difficulty,
        Username   $username,
        UtcDate    $dateCreated,
    ): self {
        return new self($name, $difficulty, $username, $dateCreated);
    }

    public function getName(): LevelName
    {
        return $this->name;
    }

    public function getDifficulty(): Difficulty
    {
        return $this->difficulty;
    }

    public function getUsername(): Username
    {
        return $this->username;
    }

    public function getDateCreated(): UtcDate
    {
        return $this->dateCreated;
    }

    public function jsonSerialize(): array
    {
        return [
            'name'        => $this->name,
            'difficulty'  => $this->difficulty->toString(),
            'user'        => $this->username,
            'dateCreated' => $this->dateCreated,
        ];
    }
}
