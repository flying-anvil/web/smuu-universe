<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Content\DataObject\Collection;

use Countable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\SmuuUniverse\Content\DataObject\Level;
use Generator;
use IteratorAggregate;

class Levels implements DataObject, IteratorAggregate, Countable
{
    /** @var Level[] */
    private array $levels;

    private function __construct(array $levels)
    {
        $this->levels = $levels;
    }

    public static function create(Level ...$levels): self
    {
        return new self($levels);
    }

    public function jsonSerialize(): array
    {
        return $this->levels;
    }

    /**
     * @return Generator<Level> | Level[]
     */
    public function getIterator(): Generator
    {
        yield from $this->levels;
    }

    public function count(): int
    {
        return count($this->levels);
    }
}
