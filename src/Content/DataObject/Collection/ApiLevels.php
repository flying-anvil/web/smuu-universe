<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Content\DataObject\Collection;

use Countable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\SmuuUniverse\Content\DataObject\ApiLevel;
use Generator;
use IteratorAggregate;

class ApiLevels implements DataObject, IteratorAggregate, Countable
{
    /** @var ApiLevel[] */
    private array $apiLevels;

    private function __construct(array $apiLevels)
    {
        $this->apiLevels = $apiLevels;
    }

    public static function create(ApiLevel ...$apiLevels): self
    {
        return new self($apiLevels);
    }

    public function jsonSerialize(): array
    {
        return $this->apiLevels;
    }

    /**
     * @return Generator<ApiLevel> | ApiLevel[]
     */
    public function getIterator(): Generator
    {
        yield from $this->apiLevels;
    }

    public function count(): int
    {
        return count($this->apiLevels);
    }
}
