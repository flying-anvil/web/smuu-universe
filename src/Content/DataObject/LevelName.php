<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Content\DataObject;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use Stringable;

class LevelName implements DataObject, StringValue, Stringable
{
    public const NAME_LENGTH_MAX = 32;

    private function __construct(private string $name)
    {
        if (mb_strlen($name) > self::NAME_LENGTH_MAX) {
            throw new ValueException(sprintf(
                'Name must be no longer than %d characters, %d given',
                self::NAME_LENGTH_MAX,
                mb_strlen($name),
            ));
        }
    }

    public static function create(string $name): self
    {
        return new self($name);
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function toString(): string
    {
        return $this->name;
    }

    public function jsonSerialize(): string
    {
        return $this->name;
    }
}
