<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Content\Repository;

use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\SmuuUniverse\Content\DataObject\Difficulty;
use FlyingAnvil\SmuuUniverse\Content\DataObject\DifficultyName;
use PDO;

class DifficultyRepository
{
    public function __construct(
        private PDO $pdo,
    ) {}

    public function loadDifficultyById(SmallId $id): ?Difficulty
    {
        $sql = <<< SQL
            SELECT * FROM difficulties
            WHERE difficulty_id = :id
        SQL;

        $statement = $this->pdo->prepare($sql);
        $statement->execute([
            'id' => $id,
        ]);

        $row = $statement->fetch();

        if ($row === false) {
            return null;
        }

        return $this->createDifficultyFromRow($row);
    }

    public function loadDifficultyByName(DifficultyName $name, int $level): ?Difficulty
    {
        $sql = <<< SQL
            SELECT * FROM difficulties
            WHERE name = :name
                AND level = :level
        SQL;

        $statement = $this->pdo->prepare($sql);
        $statement->execute([
            'name'  => $name,
            'level' => $level,
        ]);

        $row = $statement->fetch();

        if ($row === false) {
            return null;
        }

        return $this->createDifficultyFromRow($row);
    }

    private function createDifficultyFromRow(array $row): Difficulty
    {
        return Difficulty::create(
            SmallId::createFromString($row['difficulty_id']),
            DifficultyName::create($row['difficulty_id']),
            (int)$row['level'],
            (int)$row['score'],
        );
    }
}
