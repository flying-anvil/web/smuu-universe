<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Content\Repository;

use FlyingAnvil\Libfa\DataObject\Id\LongId;
use FlyingAnvil\Libfa\DataObject\Id\MediumId;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\SmuuUniverse\Content\DataObject\ApiLevel;
use FlyingAnvil\SmuuUniverse\Content\DataObject\Collection\ApiLevels;
use FlyingAnvil\SmuuUniverse\Content\DataObject\Collection\Levels;
use FlyingAnvil\SmuuUniverse\Content\DataObject\Difficulty;
use FlyingAnvil\SmuuUniverse\Content\DataObject\DifficultyName;
use FlyingAnvil\SmuuUniverse\Content\DataObject\Level;
use FlyingAnvil\SmuuUniverse\Content\DataObject\LevelName;
use FlyingAnvil\SmuuUniverse\Content\DataObject\LevelSearch;
use FlyingAnvil\SmuuUniverse\Exception\DuplicateEntryException;
use FlyingAnvil\SmuuUniverse\User\DataObject\User;
use FlyingAnvil\SmuuUniverse\User\DataObject\Username;
use PDO;
use PDOException;

class LevelRepository
{
    public function __construct(
        private PDO $pdo,
    ) {}

    public function insertNewLevel(Level $level, User $user): void
    {
        $sql = <<< SQL
            INSERT INTO levels (level_id, name, difficulty_id, user_id, date_created)
            VALUE (:levelId, :name, :difficulty, :user, :dateCreated)
        SQL;

        $statement = $this->pdo->prepare($sql);

        try {
            $statement->execute([
                'levelId'     => $level->getId(),
                'name'        => $level->getName(),
                'difficulty'  => $level->getDifficultyId(),
                'user'        => $user->getId(),
                'dateCreated' => $level->getDateCreated()->format(UtcDate::FORMAT_HUMAN),
            ]);
        } catch (PDOException $exception) {
            // Duplicate entry
            if ($exception->getCode() === '23000') {
                $errorMessage = sprintf(
                    'Level with name "%s" already exists for user "%s"',
                    $level->getName(),
                    $user->getName(),
                );

                throw new DuplicateEntryException($errorMessage, previous: $exception);
            }

            throw $exception;
        }
    }

    public function searchApiLevels(LevelSearch $search): ApiLevels
    {
        $sql = <<< SQL
            SELECT l.name AS level_name, l.date_created,
                   u.name AS user_name,
                   d.name AS d_name, d.difficulty_id AS d_id, d.level AS d_level, d.score AS d_score
            FROM levels l
            JOIN users u on u.user_id = l.user_id
            JOIN difficulties d on l.difficulty_id = d.difficulty_id
            WHERE %s
            LIMIT %d OFFSET %d
        SQL;

        $parameters = [];
        $conditions = ['1=1'];

        $levelName = $search->getLevelName();
        if ($levelName) {
            $conditions[] = 'l.name = :levelName';
            $parameters['levelName'] = $levelName;
        }

        $difficultyId = $search->getDifficultyId();
        if ($difficultyId) {
            $conditions[] = 'l.difficulty_id = :difficultyId';
            $parameters['difficultyId'] = $difficultyId;
        }

        $userName = $search->getUsername();
        if ($userName) {
            $conditions[] = 'u.name = :userName';
            $parameters['userName'] = $userName;
        }

        $dateFrom = $search->getDateFrom();
        if ($dateFrom) {
            $conditions[] = 'DATE(l.date_created) >= :dateFrom';
            $parameters['dateFrom'] = $dateFrom;
        }

        $dateTo = $search->getDateTo();
        if ($dateTo) {
            $conditions[] = 'DATE(l.date_created) <= :dateTo';
            $parameters['dateTo'] = $dateTo;
        }

        $sql = sprintf(
            $sql,
            implode(' AND ', $conditions),
            $search->getPerPage(),
            ($search->getPage() - 1) * $search->getPerPage(),
        );

        $statement = $this->pdo->prepare($sql);
        $statement->execute($parameters);

        $levels = [];
        foreach ($statement as $row) {
            $levels[] = ApiLevel::create(
                LevelName::create($row['level_name']),
                Difficulty::create(
                    SmallId::createFromString($row['d_id']),
                    DifficultyName::create($row['d_name']),
                    (int)$row['d_level'],
                    (int)$row['d_score'],
                ),
                Username::create($row['user_name']),
                UtcDate::parse($row['date_created']),
            );
        }

        return ApiLevels::create(...$levels);
    }
}
