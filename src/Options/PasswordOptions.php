<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Options;

class PasswordOptions
{
    public function __construct(
        private string $pepper,
    ) {}

    public function getPepper(): string
    {
        return $this->pepper;
    }
}
