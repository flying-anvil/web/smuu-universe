<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Options\Factory;

use FlyingAnvil\SmuuUniverse\Options\PasswordOptions;
use Psr\Container\ContainerInterface;

class PasswordOptionsFactory
{
    public function __invoke(ContainerInterface $container): PasswordOptions
    {
        $config = $container->get('passwordOptions');

        return new PasswordOptions(
            $config['pepper'],
        );
    }
}
