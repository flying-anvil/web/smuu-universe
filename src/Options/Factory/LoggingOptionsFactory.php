<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\Options\Factory;

use FlyingAnvil\SmuuUniverse\Options\LoggingOptions;
use Psr\Container\ContainerInterface;

class LoggingOptionsFactory
{
    public function __invoke(ContainerInterface $container): LoggingOptions
    {
        $config = $container->get('loggingOptions');

        return new LoggingOptions(
            $config['logLevel'],
            $config['logDirectory'],
        );
    }
}
