<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse;

use DI\Container;
use DI\ContainerBuilder;
use FlyingAnvil\Libfa\DataObject\Application\AppEnv;
use FlyingAnvil\SmuuUniverse\Client\Factory\PdoFactory;
use FlyingAnvil\SmuuUniverse\Logger\Factory\LoggerFactory;
use FlyingAnvil\SmuuUniverse\Options\Factory\LoggingOptionsFactory;
use FlyingAnvil\SmuuUniverse\Options\Factory\PasswordOptionsFactory;
use FlyingAnvil\SmuuUniverse\Options\LoggingOptions;
use FlyingAnvil\SmuuUniverse\Options\PasswordOptions;
use Monolog\Logger;
use PDO;
use Psr\Log\LoggerInterface;

use function DI\env;
use function DI\factory;
use function DI\get;

final class ContainerFactory
{
    public static function create(): Container
    {
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->addDefinitions([
            AppEnv::class          => AppEnv::createFromEnvironmentVariable(),
            Logger::class          => factory(LoggerFactory::class),
            LoggerInterface::class => get(Logger::class),

            PDO::class => factory(PdoFactory::class),

            LoggingOptions::class  => factory(LoggingOptionsFactory::class),
            PasswordOptions::class => factory(PasswordOptionsFactory::class),

            'loggingOptions' => [
                'logLevel'     => env('LOG_LEVEL', 'INFO'),
                'logDirectory' => env('LOG_DIRECTORY'),
            ],

            'passwordOptions' => [
                'pepper' => env('PASSWORD_PEPPER'),
            ],
        ]);

        return $containerBuilder->build();
    }
}
