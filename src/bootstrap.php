<?php

use FlyingAnvil\SmuuUniverse\ContainerFactory;
use FlyingAnvil\SmuuUniverse\Slim\SlimAppFactory;

require_once __DIR__ . '/../vendor/autoload.php';

(static function () {
    $root = dirname(__DIR__);
    define('PUBLIC_ROOT', $root . '/public');
    chdir($root);

    $container = ContainerFactory::create();
    $app = SlimAppFactory::create($container);
    $app->run();
})();

