<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse;

class Index
{
    public static function greet(): void
    {
        echo self::getGreeting();
    }

    public static function getGreeting(): string
    {
        $greeting = sprintf(
            'Hello World from PHP %s.%s.%s',
            PHP_MAJOR_VERSION,
            PHP_MINOR_VERSION,
            PHP_RELEASE_VERSION,
        );

        echo '<br/>';

        $time = sprintf(
            'Server time: %s',
            date(DATE_ATOM),
        );

        return sprintf(
            '%s<br/>%s',
            $greeting,
            $time,
        );
    }
}
