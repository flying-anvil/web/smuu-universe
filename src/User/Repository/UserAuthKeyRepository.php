<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\Repository;

use FlyingAnvil\Libfa\DataObject\Id\EnormousId;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\SmuuUniverse\Exception\DuplicateEntryException;
use FlyingAnvil\SmuuUniverse\User\DataObject\AuthKey;
use FlyingAnvil\SmuuUniverse\User\DataObject\User;
use PDO;
use PDOException;

class UserAuthKeyRepository
{
    public function __construct(
        private PDO $pdo,
    ) {}

    public function insertNewAuthKey(AuthKey $authKey, User $user, bool $doExpirationCheck = true): void
    {
        $sql = <<< SQL
            INSERT INTO `user_auth_keys` (`key`, name, user_id, date_created, date_expire)
            VALUE (:key, :name, :userId, :dateCreated, :dateExpire)
        SQL;

        $statement = $this->pdo->prepare($sql);

        try {
            $statement->execute([
                'key'         => hash('sha256', $authKey->getKey()->toString()),
                'name'        => $authKey->getName(),
                'userId'      => $user->getId(),
                'dateCreated' => $authKey->getDateCreated()->format(),
                'dateExpire'  => $authKey->getDateExpire()->format(),
            ]);
        } catch (PDOException $exception) {
            // Duplicate entry
            if ($exception->getCode() === '23000') {
                $errorMessage = sprintf(
                    'AuthKey with name "%s" already exists for user "%s"',
                    $authKey->getName(),
                    $user->getId(),
                );

                throw new DuplicateEntryException($errorMessage, previous: $exception);
            }

            throw $exception;
        }
    }

    public function loadAuthKeyByName(User $user, string $name): ?AuthKey
    {
        $sql = <<< SQL
            SELECT * FROM user_auth_keys
            WHERE `user_id` = :userId
                AND name = :name
        SQL;

        $statement = $this->pdo->prepare($sql);
        $statement->execute([
            'userId' => $user->getId(),
            'name'   => $name,
        ]);

        $row = $statement->fetch();

        if ($row === false) {
            return null;
        }

        return AuthKey::createExisting(
            EnormousId::createFromString($row['key']),
            $row['name'],
            UtcDate::parse($row['date_created']),
            UtcDate::parse($row['date_expire']),
        );
    }

    public function expireAuthKey(EnormousId $authKeyId): void
    {
        $sql = <<< SQL
            DELETE FROM user_auth_keys
            WHERE `key` = :key
        SQL;

        $this->pdo->prepare($sql)->execute([
            'key' => $authKeyId,
        ]);
    }

    public function expireAuthKeyByName(User $user, string $name): bool
    {
        $sql = <<< SQL
            DELETE FROM user_auth_keys
            WHERE name = :name
                AND user_id = :userId
        SQL;

        $statement = $this->pdo->prepare($sql);
        $statement->execute([
            'userId' => $user->getId(),
            'name'   => $name,
        ]);

        return $statement->rowCount() > 0;
    }

    public function checkExpiration(AuthKey $authKey, User $user): bool
    {
        $existingKey = $this->loadAuthKeyByName($user, $authKey->getName());

        if ($existingKey && UtcDate::now() > $existingKey->getDateExpire()) {
            $this->expireAuthKey($existingKey->getKey());
            return true;
        }

        return false;
    }
}
