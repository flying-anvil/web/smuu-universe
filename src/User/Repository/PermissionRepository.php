<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\Repository;

use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\SmuuUniverse\User\DataObject\Collection\Permissions;
use FlyingAnvil\SmuuUniverse\User\DataObject\Permission;
use FlyingAnvil\SmuuUniverse\User\DataObject\PermissionName;
use PDO;

class PermissionRepository
{
    public function __construct(
        private PDO $pdo,
    ) {}

    public function loadPermissionsForUser(SmallId $userId): Permissions
    {
        $sql = <<< SQL
            SELECT p.permission_id, p.name, p.affects_frontend
            FROM permissions p
            JOIN role_permissions rp on p.permission_id = rp.permission_id
            JOIN roles r on rp.role_id = r.role_id
            JOIN user_roles ur on r.role_id = ur.role_id
            WHERE ur.user_id = :userId
        SQL;

        $statement = $this->pdo->prepare($sql);
        $statement->execute([
            'userId' => $userId,
        ]);

        $permissions = [];
        foreach ($statement as $row) {
            $permissions[] = Permission::create(
                SmallId::createFromString($row['permission_id']),
                PermissionName::create($row['name']),
                (bool)$row['affects_frontend'],
            );
        }

        return Permissions::create(...$permissions);
    }
}
