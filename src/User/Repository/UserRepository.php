<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\Repository;

use FlyingAnvil\Libfa\DataObject\Credentials\Password;
use FlyingAnvil\Libfa\DataObject\Id\EnormousId;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\SmuuUniverse\Exception\DuplicateEntryException;
use FlyingAnvil\SmuuUniverse\Options\PasswordOptions;
use FlyingAnvil\SmuuUniverse\User\DataObject\ApiUser;
use FlyingAnvil\SmuuUniverse\User\DataObject\Collection\ApiUsers;
use FlyingAnvil\SmuuUniverse\User\DataObject\Collection\Usernames;
use FlyingAnvil\SmuuUniverse\User\DataObject\User;
use FlyingAnvil\SmuuUniverse\User\DataObject\Username;
use PDO;
use PDOException;

class UserRepository
{
    public function __construct(
        private PDO $pdo,
        private PasswordOptions $passwordOptions,
        private BadgeRepository $badgeRepository,
        private PermissionRepository $permissionRepository,
    ) {}

    public function insertNewUser(User $user): void
    {
        $sql = <<< SQL
            INSERT INTO `users` (user_id, name, password, date_joined)
            VALUE (:id, :name, :password, :dateJoined)
        SQL;

        $statement = $this->pdo->prepare($sql);

        try {
            $statement->execute([
                'id'         => $user->getId(),
                'name'       => $user->getName(),
                'password'   => $user->getPassword(),
                'dateJoined' => $user->getDateJoined(),
            ]);
        } catch (PDOException $exception) {
            // Duplicate entry
            if ($exception->getCode() === '23000') {
                $errorMessage = sprintf(
                    'User with name "%s" already exists',
                    $user->getName(),
                );

                throw new DuplicateEntryException($errorMessage, previous: $exception);
            }

            throw $exception;
        }
    }

    public function verifyUserPasswordByName(string $rawPassword, Username $username): bool
    {
        $sql = <<< SQL
            SELECT password FROM users WHERE name = :username
        SQL;

        $statement = $this->pdo->prepare($sql);
        $statement->execute([
            'username' => $username->toString(),
        ]);

        $row = $statement->fetch();

        $password = Password::createFromHash(
            $row['password'],
            $this->passwordOptions->getPepper(),
        );

        return $password->isValid($rawPassword);
    }

    public function loadUserByName(Username $username): User
    {
        $sql = <<< SQL
            SELECT user_id, password, date_joined FROM users WHERE name = :username
        SQL;

        $statement = $this->pdo->prepare($sql);
        $statement->execute([
            'username' => $username->toString(),
        ]);

        $row = $statement->fetch();

        $userId      = SmallId::createFromString($row['user_id']);
        $permissions = $this->permissionRepository->loadPermissionsForUser($userId);

        return User::create(
            $userId,
            $username,
            UtcDate::parse($row['date_joined']),
            Password::createFromHash($row['password'], $this->passwordOptions->getPepper()),
            $permissions,
        );
    }

    public function loadUserByAuthKeyId(EnormousId $authKeyId): ?User
    {
        $sql = <<< SQL
            SELECT u.* FROM users u
            JOIN user_auth_keys a on u.user_id = a.user_id
            WHERE a.key = :key
        SQL;

        $statement = $this->pdo->prepare($sql);
        $statement->execute([
            'key' => hash('sha256', $authKeyId->toString()),
        ]);

        $row = $statement->fetch();

        if ($row === false) {
            return null;
        }

        $userId      = SmallId::createFromString($row['user_id']);
        $permissions = $this->permissionRepository->loadPermissionsForUser($userId);
        return User::create(
            $userId,
            Username::create($row['name']),
            UtcDate::parse($row['date_joined']),
            Password::createFromHash($row['password'], $this->passwordOptions->getPepper()),
            $permissions,
        );
    }

    public function loadApiUsersByNames(Usernames $names): ApiUsers
    {
        if ($names->count() === 0) {
            return ApiUsers::create();
        }

        $sql = <<< SQL
            SELECT user_id, name, date_joined FROM users
            WHERE name IN (%s)
        SQL;

        $parameters = [];

        foreach ($names as $index => $name) {
            /** @noinspection GrazieInspection */
            $keyName = ':name' . $index;
            $parameters[$keyName] = $name->toString();
        }

        $sql = sprintf(
            $sql,
            implode(', ', array_keys($parameters)),
        );

        $statement = $this->pdo->prepare($sql);
        $statement->execute($parameters);

        $apiUsers = [];
        foreach ($statement as $row) {
            $userId = SmallId::createFromString($row['user_id']);

            $apiUsers[] = ApiUser::create(
                Username::create($row['name']),
                UtcDate::parse($row['date_joined']),
                $this->badgeRepository->loadApiBadgesForUser($userId),
            );
        }

        return ApiUsers::create(...$apiUsers);
    }
}
