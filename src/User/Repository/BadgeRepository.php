<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\Repository;

use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\SmuuUniverse\User\DataObject\Badge\ApiBadge;
use FlyingAnvil\SmuuUniverse\User\DataObject\Badge\ApiBadgeRanks;
use FlyingAnvil\SmuuUniverse\User\DataObject\Badge\BadgeDescription;
use FlyingAnvil\SmuuUniverse\User\DataObject\Badge\BadgeName;
use FlyingAnvil\SmuuUniverse\User\DataObject\Badge\BadgeRank;
use FlyingAnvil\SmuuUniverse\User\DataObject\Badge\BadgeRankDescription;
use FlyingAnvil\SmuuUniverse\User\DataObject\Badge\BadgeRankName;
use FlyingAnvil\SmuuUniverse\User\DataObject\Badge\RankName;
use FlyingAnvil\SmuuUniverse\User\DataObject\Collection\ApiBadges;
use PDO;

class BadgeRepository
{
    public function __construct(
        private PDO $pdo,
    ) {}

    public function loadAllApiBadges(): ApiBadges
    {
        $sql = <<< SQL
            SELECT b.badge_id, b.name, b.description,
                br.badge_rank_id as br_badge_rank_id, br.name AS br_name, br.description AS br_description,
                r.name AS r_name, r.order
            FROM badges b
            JOIN badge_ranks br on b.badge_id = br.badge_id
            JOIN ranks r on r.rank_id = br.rank_id
            ORDER BY b.name, r.`order`
        SQL;

        $statement = $this->pdo->query($sql);

        $badges = [];
        foreach ($statement as $row) {
            $rawBadgeName = $row['name'];
            $badgeName = BadgeName::create($rawBadgeName);

            if (!isset($badges[$rawBadgeName])) {
                $badges[$rawBadgeName] = ApiBadge::create(
                    SmallId::createFromString($row['badge_id']),
                    $badgeName,
                    BadgeDescription::create($row['description']),
                    ApiBadgeRanks::create(),
                );
            }

            $badges[$rawBadgeName]->getRanks()->add(BadgeRank::create(
                SmallId::createFromString($row['br_badge_rank_id']),
                RankName::create($row['r_name']),
                (int)$row['order'],
                BadgeRankName::create($row['br_name']),
                BadgeRankDescription::create($row['br_description']), null,
            ));
        }

        return ApiBadges::create(...array_values($badges));
    }

    public function loadApiBadgesForUser(SmallId $userId): ApiBadges
    {
        $sql = <<< SQL
            SELECT b.badge_id, b.name, b.description,
                br.badge_rank_id as br_badge_rank_id, br.name AS br_name, br.description AS br_description,
                r.name AS r_name, r.order,
                ub.date_awarded AS ub_date_awarded
            FROM badges b
            JOIN badge_ranks br on b.badge_id = br.badge_id
            JOIN ranks r on br.rank_id = r.rank_id
            JOIN user_badges ub on br.badge_rank_id = ub.badge_rank_id
            WHERE ub.user_id = :userId
            ORDER BY b.name, r.`order`
        SQL;

        $statement = $this->pdo->prepare($sql);
        $statement->execute([
            'userId' => $userId,
        ]);

        $badges = [];
        foreach ($statement as $row) {
            $rawBadgeName = $row['name'];
            $badgeName = BadgeName::create($rawBadgeName);

            if (!isset($badges[$rawBadgeName])) {
                $badges[$rawBadgeName] = ApiBadge::create(
                    SmallId::createFromString($row['badge_id']),
                    $badgeName,
                    BadgeDescription::create($row['description']),
                    ApiBadgeRanks::create(),
                );
            }

            $badges[$rawBadgeName]->getRanks()->add(BadgeRank::create(
                SmallId::createFromString($row['br_badge_rank_id']),
                RankName::create($row['r_name']),
                (int)$row['order'],
                BadgeRankName::create($row['br_name']),
                BadgeRankDescription::create($row['br_description']),
                $row['ub_date_awarded'] ? UtcDate::parse($row['ub_date_awarded']) : UtcDate::now(),
            ));
        }

        return ApiBadges::create(...array_values($badges));
    }

    public function loadAllBadgeStatistics(): array
    {
        $sql = <<< SQL
            SELECT
                   br.badge_rank_id,
                   b.name as b_name, b.badge_id as b_badge_id,
                   r.name as r_name,
                   br.name as br_name, br.description as br_description,
                   COUNT(br.badge_rank_id) as `count`
            FROM user_badges ub
            JOIN badge_ranks br on ub.badge_rank_id = br.badge_rank_id
            JOIN ranks r on br.rank_id = r.rank_id
            JOIN badges b on br.badge_id = b.badge_id
            GROUP BY br.badge_rank_id
            ORDER BY b.name, r.`order`
        SQL;

        $statement = $this->pdo->query($sql);

        $stats = [];
        foreach ($statement as $row) {
            $stats[] = [
                'badgeRankId'          => SmallId::createFromString($row['badge_rank_id']),
                'badgeName'            => BadgeName::create($row['b_name']),
                'rankName'             => RankName::create($row['r_name']),
                'badgeRankName'        => BadgeRankName::create($row['br_name']),
                'badgeRankDescription' => BadgeRankDescription::create($row['br_description']),
                'badgeId'              => SmallId::createFromString($row['b_badge_id']),
                'countAwarded'         => (int)$row['count'],
            ];
        }

        return $stats;
    }
}
