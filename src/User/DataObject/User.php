<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject;

use FlyingAnvil\Libfa\DataObject\Credentials\Password;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\SmuuUniverse\User\DataObject\Collection\Permissions;

final class User implements DataObject
{
    private function __construct(
        private SmallId $id,
        private Username $name,
        private UtcDate $dateJoined,
        private Password $password,
        private Permissions $permissions,
    ) {}

    public static function create(SmallId $id, Username $name, UtcDate $dateJoined, Password $password, Permissions $permissions): self
    {
        return new self($id, $name, $dateJoined, $password, $permissions);
    }

    public function getId(): SmallId
    {
        return $this->id;
    }

    public function getName(): Username
    {
        return $this->name;
    }

    public function getDateJoined(): UtcDate
    {
        return $this->dateJoined;
    }

    public function getPassword(): Password
    {
        return $this->password;
    }

    public function hasPermission(string $permissionName): bool
    {
        return $this->permissions->hasPermission($permissionName);
    }

    public function getPermissions(): Permissions
    {
        return $this->permissions;
    }

    public function jsonSerialize(): array
    {
        return [
            'name'        => $this->name,
            'dateJoined'  => $this->dateJoined,
            'permissions' => $this->permissions->getFrontendPermissions(),
        ];
    }
}
