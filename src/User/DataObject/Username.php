<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\Libfa\DataObject\Math\Range;
use JetBrains\PhpStorm\Immutable;
use Stringable;

#[Immutable]
final class Username implements DataObject, StringValue, Stringable
{
    public const LENGTH_MIN = 3;
    public const LENGTH_MAX = 32;

    private function __construct(private string $username)
    {
        $range  = Range::create(self::LENGTH_MIN, self::LENGTH_MAX);
        $length = mb_strlen($this->username);

        if (!$range->check($length)) {
            throw new ValueException(sprintf(
                'Username length is invalid, must be in range %s, %d given',
                $range,
                $length,
            ));
        }
    }

    public static function create(string $username): self
    {
        return new self($username);
    }

    public function toString(): string
    {
        return $this->username;
    }

    public function __toString(): string
    {
        return $this->username;
    }

    public function jsonSerialize(): string
    {
        return $this->username;
    }
}
