<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject;

use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\Libfa\DataObject\Id\EnormousId;
use FlyingAnvil\Libfa\DataObject\Math\Range;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class AuthKey
{
    public const NAME_LENGTH_MAX = 32;

    private function __construct(
        private EnormousId $key,
        private string $name,
        private UtcDate $dateCreated,
        private UtcDate $dateExpire,
    ) {
        $range  = Range::create(0, self::NAME_LENGTH_MAX);
        $length = mb_strlen($this->name);

        if (!$range->check($length)) {
            throw new ValueException(sprintf(
                'Description length is invalid, must be in range %s, %d given',
                $range,
                $length,
            ));
        }
    }

    public static function createExisting(EnormousId $key, string $name, UtcDate $dateCreated, UtcDate $dateExpire): self
    {
        return new self($key, $name, $dateCreated, $dateExpire);
    }

    public static function generate(string $name, UtcDate $dateExpire): self
    {
        return new self(
            EnormousId::generate(),
            $name,
            UtcDate::now(),
            $dateExpire,
        );
    }

    public function getKey(): EnormousId
    {
        return $this->key;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDateCreated(): UtcDate
    {
        return $this->dateCreated;
    }

    public function getDateExpire(): UtcDate
    {
        return $this->dateExpire;
    }
}
