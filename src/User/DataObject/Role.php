<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use JetBrains\PhpStorm\Immutable;
use Stringable;

#[Immutable]
final class Role implements DataObject, StringValue, Stringable
{
    private function __construct(
        private SmallId  $id,
        private RoleName $name,
    ) {}

    public function __toString(): string
    {
        return $this->name->toString();
    }

    public function toString(): string
    {
        return $this->name->toString();
    }

    public function jsonSerialize(): array
    {
        return [
            'id'   => $this->id,
            'name' => $this->name,
        ];
    }
}
