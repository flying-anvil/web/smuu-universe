<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject;

use FlyingAnvil\Libfa\DataObject\Credentials\Password;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\SmuuUniverse\User\DataObject\Badge\ApiBadge;
use FlyingAnvil\SmuuUniverse\User\DataObject\Collection\ApiBadges;

final class ApiUser implements DataObject
{
    private function __construct(
        private Username $name,
        private UtcDate $dateJoined,
        private ApiBadges $badges,
    ) {}

    public static function create(Username $name, UtcDate $dateJoined, ApiBadges $badges): self
    {
        return new self($name, $dateJoined, $badges);
    }

    public function jsonSerialize(): array
    {
        return [
            'name'       => $this->name,
            'dateJoined' => $this->dateJoined,
            'badges'     => $this->badges,
        ];
    }
}
