<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject\Collection;

use Countable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\SmuuUniverse\User\DataObject\Username;
use Generator;
use IteratorAggregate;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
class Usernames implements DataObject, IteratorAggregate, Countable
{
    /** @var Username[] */
    private array $usernames;

    private function __construct(array $usernames)
    {
        $this->usernames = $usernames;
    }

    public static function create(Username ...$usernames): self
    {
        return new self($usernames);
    }

    public function jsonSerialize(): array
    {
        return $this->usernames;
    }

    /**
     * @return Generator<Username> | Username[]
     */
    public function getIterator(): Generator
    {
        yield from $this->usernames;
    }

    public function count(): int
    {
        return count($this->usernames);
    }
}
