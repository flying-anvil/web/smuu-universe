<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject\Collection;

use Countable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\SmuuUniverse\User\DataObject\ApiUser;
use Generator;
use IteratorAggregate;

class ApiUsers implements DataObject, IteratorAggregate, Countable
{
    /** @var ApiUser[] */
    private array $apiUsers;

    private function __construct(array $apiUsers)
    {
        $this->apiUsers = $apiUsers;
    }

    public static function create(ApiUser ...$apiUsers): self
    {
        return new self($apiUsers);
    }

    public function jsonSerialize(): array
    {
        return $this->apiUsers;
    }

    /**
     * @return Generator<ApiUser> | ApiUser[]
     */
    public function getIterator(): Generator
    {
        yield from $this->apiUsers;
    }

    public function count(): int
    {
        return count($this->apiUsers);
    }
}
