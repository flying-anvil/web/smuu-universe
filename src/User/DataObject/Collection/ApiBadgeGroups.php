<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject\Collection;

use Countable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\SmuuUniverse\User\DataObject\Badge\ApiBadgeGroup;
use FlyingAnvil\SmuuUniverse\User\DataObject\Badge\BadgeGroupDescription;
use FlyingAnvil\SmuuUniverse\User\DataObject\Badge\BadgeGroupName;
use Generator;
use IteratorAggregate;

class ApiBadgeGroups implements DataObject, IteratorAggregate, Countable
{
    /** @var ApiBadgeGroup[] */
    private array $apiBadgeGroups;

    /**
     * @param ApiBadgeGroup[] $apiBadgeGroups
     */
    private function __construct(array $apiBadgeGroups)
    {
        $this->apiBadgeGroups = [];

        foreach ($apiBadgeGroups as $group) {
            $this->apiBadgeGroups[$group->getGroupName()->toString()] = $group;
        }
    }

    public static function create(ApiBadgeGroup ...$apiBadgeGroups): self
    {
        return new self($apiBadgeGroups);
    }

    public static function createFromIndexedBadgeArray(array $apiBadgeGroups): self
    {
        $groups = [];

        foreach ($apiBadgeGroups as $rawGroup) {
            $groups[] = ApiBadgeGroup::create(
                BadgeGroupName::create($rawGroup['groupName']),
                BadgeGroupDescription::create($rawGroup['description']),
                ApiBadges::create(...$rawGroup['badges']),
            );
        }

        return self::create(...$groups);
    }

    public function jsonSerialize(): array
    {
        return $this->apiBadgeGroups;
    }

    /**
     * @return Generator<ApiBadgeGroup> | ApiBadgeGroup[]
     */
    public function getIterator(): Generator
    {
        yield from $this->apiBadgeGroups;
    }

    public function count(): int
    {
        return count($this->apiBadgeGroups);
    }
}
