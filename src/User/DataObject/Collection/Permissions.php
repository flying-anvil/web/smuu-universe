<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject\Collection;

use Countable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\SmuuUniverse\User\DataObject\Permission;
use Generator;
use IteratorAggregate;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class Permissions implements DataObject, IteratorAggregate, Countable
{
    /** @var Permission[] */
    private array $permissions;

    private function __construct(array $permissions)
    {
        $this->permissions = [];

        foreach ($permissions as $permission) {
            $this->permissions[$permission->toString()] = $permission;
        }
    }

    public static function create(Permission ...$permissions): self
    {
        return new self($permissions);
    }

    public function hasPermission(string $permissionName): bool
    {
        return isset($this->permissions[$permissionName]);
    }

    public function getFrontendPermissions(): Permissions
    {
        $permissions = [];

        foreach ($this->permissions as $permission) {
            if ($permission->isAffectingFrontend()) {
                $permissions[] = $permission;
            }
        }

        return new self($permissions);
    }

    public function jsonSerialize(): array
    {
        return $this->permissions;
    }

    /**
     * @return Generator<Permission> | Permission[]
     */
    public function getIterator(): Generator
    {
        yield from $this->permissions;
    }

    public function count(): int
    {
        return count($this->permissions);
    }

    /**
     * @return string[]
     */
    public function toStringArray(): array
    {
        return array_keys($this->permissions);
    }
}
