<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject\Collection;

use Countable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\SmuuUniverse\User\DataObject\Badge\ApiBadge;
use Generator;
use IteratorAggregate;

class ApiBadges implements DataObject, IteratorAggregate, Countable
{
    /** @var ApiBadge[] */
    private array $apiBadges;

    private function __construct(array $apiBadges)
    {
        $this->apiBadges = $apiBadges;
    }

    public static function create(ApiBadge ...$apiBadges): self
    {
        return new self($apiBadges);
    }

    public function jsonSerialize(): array
    {
        return $this->apiBadges;
    }

    /**
     * @return Generator<ApiBadge> | ApiBadge[]
     */
    public function getIterator(): Generator
    {
        yield from $this->apiBadges;
    }

    public function count(): int
    {
        return count($this->apiBadges);
    }
}
