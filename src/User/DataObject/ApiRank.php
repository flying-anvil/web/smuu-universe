<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\SmuuUniverse\User\DataObject\Badge\RankName;
use JetBrains\PhpStorm\Immutable;
use Stringable;

#[Immutable]
final class ApiRank implements DataObject, StringValue, Stringable
{
    private function __construct(
        private RankName $rankName,
        private int $order,
    ) {}

    public static function create(RankName $rankName, int $order): self
    {
        return new self($rankName, $order);
    }

    public function __toString(): string
    {
        return $this->rankName->toString();
    }

    public function toString(): string
    {
        return $this->rankName->toString();
    }

    public function jsonSerialize(): array
    {
        return [
            'name'  => $this->rankName,
            'order' => $this->order,
        ];
    }
}
