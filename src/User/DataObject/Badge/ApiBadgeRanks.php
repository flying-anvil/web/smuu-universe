<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject\Badge;

use Countable;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\SmuuUniverse\DataObject\Image;
use FlyingAnvil\SmuuUniverse\DataObject\Url;
use Generator;
use IteratorAggregate;

final class ApiBadgeRanks implements DataObject, IteratorAggregate, Countable
{
    /**
     * @param BadgeRank[] $apiBadgeRanks
     */
    private function __construct(private array $apiBadgeRanks) {}

    public static function create(BadgeRank ...$apiBadgeRanks): self
    {
        return new self($apiBadgeRanks);
    }

    public function add(BadgeRank $badgeRank): void
    {
        $this->apiBadgeRanks[] = $badgeRank;
    }

    public function jsonSerialize(): array
    {
        return $this->apiBadgeRanks;
    }

    /**
     * @return Generator<BadgeRank> | BadgeRank[]
     */
    public function getIterator(): Generator
    {
        yield from $this->apiBadgeRanks;
    }

    public function count(): int
    {
        return count($this->apiBadgeRanks);
    }
}
