<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject\Badge;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\SmuuUniverse\DataObject\Image;
use FlyingAnvil\SmuuUniverse\DataObject\Url;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class BadgeRank implements DataObject
{
    private Image $image;

    private function __construct(
        private SmallId              $id,
        private RankName             $rankName,
        private int                  $order,
        private BadgeRankName        $badgeRankName,
        private BadgeRankDescription $badgeRankDescription,
        private ?UtcDate             $dateAwarded = null,
    ) {
        $this->image = $this->getImage();
    }

    public static function create(
        SmallId              $id,
        RankName             $rankName,
        int                  $order,
        BadgeRankName        $badgeRankName,
        BadgeRankDescription $badgeRankDescription,
        ?UtcDate             $dateAwarded = null,
    ): self {
        return new self($id, $rankName, $order, $badgeRankName, $badgeRankDescription, $dateAwarded);
    }

    public function getRankName(): RankName
    {
        return $this->rankName;
    }

    public function getOrder(): int
    {
        return $this->order;
    }

    public function getBadgeRankName(): BadgeRankName
    {
        return $this->badgeRankName;
    }

    public function getBadgeRankDescription(): BadgeRankDescription
    {
        return $this->badgeRankDescription;
    }

    public function getDateAwarded(): ?UtcDate
    {
        return $this->dateAwarded;
    }

    public function jsonSerialize(): array
    {
        return [
            'badgeRankId'          => $this->id,
            'rankName'             => $this->rankName,
            'order'                => $this->order,
            'badgeRankName'        => $this->badgeRankName,
            'badgeRankDescription' => $this->badgeRankDescription,
            'dateAwarded'          => $this->dateAwarded,
            'image'                => $this->image,
        ];
    }

    private function getImage(): Image
    {
        $path = sprintf(
            '/images/badges/%s.png',
            $this->id,
        );

        if (file_exists(PUBLIC_ROOT . $path)) {
            return Image::create(
                Url::create($path),
                $this->badgeRankName->toString(),
            );
        }

        return Image::create(
            Url::create('/images/badges/Missing.png'),
            'Missing',
            false,
        );
    }
}
