<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject\Badge;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use JetBrains\PhpStorm\Immutable;
use Stringable;

#[Immutable]
final class Badge implements DataObject, StringValue, Stringable
{
    private function __construct(
        private SmallId $id,
        private BadgeName $name,
        private BadgeDescription $description,
    ) {}

    public function getId(): SmallId
    {
        return $this->id;
    }

    public function getName(): BadgeName
    {
        return $this->name;
    }

    public function getDescription(): BadgeDescription
    {
        return $this->description;
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function toString(): string
    {
        return sprintf(
            '%s: %s',
            $this->name,
            $this->description,
        );
    }

    public function jsonSerialize(): array
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'description' => $this->description,
        ];
    }
}
