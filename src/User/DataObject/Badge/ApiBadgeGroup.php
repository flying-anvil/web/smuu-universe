<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject\Badge;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\SmuuUniverse\User\DataObject\Collection\ApiBadges;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class ApiBadgeGroup implements DataObject
{
    private function __construct(
        private BadgeGroupName $groupName,
        private BadgeGroupDescription $groupDescription,
        private ApiBadges $badges,
    ) {}

    public static function create(BadgeGroupName $groupName, BadgeGroupDescription $groupDescription, ApiBadges $badges): self
    {
        return new self($groupName, $groupDescription, $badges);
    }

    public function getGroupName(): BadgeGroupName
    {
        return $this->groupName;
    }

    public function getGroupDescription(): BadgeGroupDescription
    {
        return $this->groupDescription;
    }

    public function getBadges(): ApiBadges
    {
        return $this->badges;
    }

    public function jsonSerialize(): array
    {
        return [
            'groupName'        => $this->groupName,
            'groupDescription' => $this->groupDescription,
            'badges'           => $this->badges,
        ];
    }
}
