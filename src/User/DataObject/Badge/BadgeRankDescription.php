<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject\Badge;

use FlyingAnvil\SmuuUniverse\DataObject\AbstractStringDataObject;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class BadgeRankDescription extends AbstractStringDataObject
{
    protected function getMaxLength(): int
    {
        return 255;
    }
}
