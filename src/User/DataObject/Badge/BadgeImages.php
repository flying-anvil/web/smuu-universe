<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject\Badge;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\SmuuUniverse\DataObject\Image;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class BadgeImages implements DataObject
{
    private function __construct(
        private Image $small,
        private Image $medium,
        private Image $large,
    ) {}

    public static function create(Image $small, Image $medium, Image $large): self
    {
        return new self($small, $medium, $large);
    }

    public function jsonSerialize(): array
    {
        return [
            'small'  => $this->small,
            'medium' => $this->medium,
            'large'  => $this->large,
        ];
    }
}
