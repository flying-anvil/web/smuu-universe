<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject\Badge;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\SmuuUniverse\DataObject\Image;
use FlyingAnvil\SmuuUniverse\DataObject\Url;
use JetBrains\PhpStorm\Immutable;
use Stringable;

#[Immutable]
final class ApiBadge implements DataObject, StringValue, Stringable
{
//    private BadgeImages $badgeImages;

    private function __construct(
        private SmallId          $id,
        private BadgeName        $name,
        private BadgeDescription $description,
        private ApiBadgeRanks    $ranks,
    ) {
//        $imageSmall  = $this->getImage('small');
//        $imageMedium = $this->getImage('medium');
//        $imageLarge  = $this->getImage('large');
//
//        $this->badgeImages = BadgeImages::create(
//            $imageSmall,
//            $imageMedium,
//            $imageLarge,
//        );
    }

    public static function create(
        SmallId          $id,
        BadgeName        $name,
        BadgeDescription $description,
        ApiBadgeRanks    $ranks,
    ): self {
        return new self($id, $name, $description, $ranks);
    }

    public function getId(): SmallId
    {
        return $this->id;
    }

    public function getName(): BadgeName
    {
        return $this->name;
    }

    public function getDescription(): BadgeDescription
    {
        return $this->description;
    }

    public function getRanks(): ApiBadgeRanks
    {
        return $this->ranks;
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function toString(): string
    {
        return sprintf(
            '%s: %s',
            $this->name,
            $this->description,
        );
    }

    public function jsonSerialize(): array
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'ranks'       => $this->ranks,
            'description' => $this->description,
        ];
    }
}
