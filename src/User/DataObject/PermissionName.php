<?php

declare(strict_types=1);

namespace FlyingAnvil\SmuuUniverse\User\DataObject;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\Libfa\DataObject\Math\Range;
use FlyingAnvil\SmuuUniverse\DataObject\AbstractStringDataObject;
use JetBrains\PhpStorm\Immutable;
use Stringable;

#[Immutable]
final class PermissionName extends AbstractStringDataObject
{
    protected function getMaxLength(): int
    {
        return 64;
    }
}
