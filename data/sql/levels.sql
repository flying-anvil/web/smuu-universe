CREATE TABLE `levels`
(
    `id`            int unsigned NOT NULL AUTO_INCREMENT,
    `level_id`      char(16)     NOT NULL,
    `name`          varchar(32)  NOT NULL,
    `difficulty_id` char(8)      NOT NULL,
    `user_id`       char(8)      NOT NULL,
    `world_id`      char(12)     NULL,
    `date_created`  DATETIME     NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uq_level_id` (`level_id`),
    UNIQUE KEY `uq_name_user` (`name`, `user_id`),
    KEY `key_name` (`name`),
    KEY `key_difficulty` (`difficulty_id`),
    KEY `key_user` (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
