CREATE TABLE IF NOT EXISTS `role_permissions`
(
    `role_id`       char(8) NOT NULL,
    `permission_id` char(8) NOT NULL,
    `deny`          boolean NOT NULL DEFAULT false,
    PRIMARY KEY (`role_id`, `permission_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

INSERT IGNORE INTO `role_permissions` (role_id, permission_id)
VALUES
# TODO: Put sane defaults here

       # Moderator
       ('9VAgzbfs', 'VJZlDVsd'), # forum.post.delete
       ('9VAgzbfs', 'YnF1bVuU'), # forum.post.hide
       ('9VAgzbfs', '0zSZesD6'), # forum.post.show
       ('9VAgzbfs', 'FVIkK3Yq'), # forum.post.feature

       ('9VAgzbfs', 'nthlolUK'), # forum.tutorial.delete
       ('9VAgzbfs', 'tBr6R53F'), # forum.tutorial.hide
       ('9VAgzbfs', 'Jglf3tsU'), # forum.tutorial.show
       ('9VAgzbfs', '12TNAnwk'), # forum.tutorial.approve
       ('9VAgzbfs', 'q6gcsRhQ'), # forum.tutorial.feature

       ('9VAgzbfs', 'zy6y9IX7'), # content.level.delete
       ('9VAgzbfs', 'NI0RCDW2'), # content.level.hide
       ('9VAgzbfs', 'Iv51vTlY'), # content.level.show
       ('9VAgzbfs', '0B6Ro6j2'), # content.level.approve
       ('9VAgzbfs', 'ulggwGpa'), # content.level.feature

       ('9VAgzbfs', 'WP24GmI3'), # content.world.delete
       ('9VAgzbfs', 'pYUmtGmg'), # content.world.hide
       ('9VAgzbfs', '07BVzsis'), # content.world.show
       ('9VAgzbfs', 'zoU3Kuga'), # content.world.approve
       ('9VAgzbfs', 'RDY7Sz0y'), # content.world.feature

       # Admin
       ('rLX8sQ89', 'YFhtEi0t'), # user.create
       ('rLX8sQ89', 'btcsGpry'), # user.delete
       ('rLX8sQ89', '26NdPOju'), # user.modify
       ('rLX8sQ89', 'XOUhYKBa'), # user.change_roles
       ('rLX8sQ89', 'H5niw4dw'), # role.create
       ('rLX8sQ89', '8VZvaP84'), # role.delete
       ('rLX8sQ89', 'eB8NJvYv'), # role.modify
       ('rLX8sQ89', 'u7XwmKbt'), # permission.create
       ('rLX8sQ89', 'EvaIYekJ'), # permission.delete
       ('rLX8sQ89', '182YidEe'), # permission.modify
       ('rLX8sQ89', 'B8Uawdp5'), # badge.create
       ('rLX8sQ89', '3NLWDDIn'), # badge.delete
       ('rLX8sQ89', 'mtHPcE06'), # badge.modify
       ('rLX8sQ89', 'LakVbFAg'), # tip.create
       ('rLX8sQ89', 'lr9zWWRm'), # tip.delete
       ('rLX8sQ89', '98BOMPSb'); # tip.modify

INSERT IGNORE INTO `role_permissions` (role_id, permission_id, deny)
VALUES
       # Troll
       ('aB0jxVEm', '9QPfMt6x', true), # forum.post.create
       ('aB0jxVEm', 'QXOHxpSY', true); # forum.tutorial.create
