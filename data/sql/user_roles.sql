CREATE TABLE IF NOT EXISTS `user_roles`
(
    `user_id` char(8) NOT NULL,
    `role_id` char(8) NOT NULL,
    PRIMARY KEY (`user_id`, `role_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
