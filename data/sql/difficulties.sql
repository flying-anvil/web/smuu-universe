CREATE TABLE IF NOT EXISTS `difficulties`
(
    `difficulty_id` char(8)          NOT NULL,
    `name`          varchar(16)      NOT NULL,
    `level`         tinyint unsigned NOT NULL,
    `score`         tinyint unsigned NOT NULL,
    PRIMARY KEY (`difficulty_id`),
    UNIQUE KEY `uq_difficulty` (`name`, `level`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

INSERT INTO difficulties (difficulty_id, name, level, score)
VALUES ('RjgR5GCw', 'easy', 1, 1),
       ('7rX2ysyc', 'easy', 2, 2),
       ('WXIe1gfQ', 'easy', 3, 3),
       ('zWXF29d3', 'easy', 4, 4),
       ('QQcZKQ0G', 'easy', 5, 5),

       ('DHGeIxY7', 'medium', 1, 6),
       ('yARzy8dN', 'medium', 2, 7),
       ('XciI4j3f', 'medium', 3, 8),
       ('vFZPhZwu', 'medium', 4, 9),
       ('wUs7UKOK', 'medium', 5, 10),

       ('Ez9KBkHw', 'hard', 1, 11),
       ('2eDfYkXh', 'hard', 2, 12),
       ('YAxfH2XR', 'hard', 3, 13),
       ('TP5Hfv1V', 'hard', 4, 14),
       ('AqjFezDB', 'hard', 5, 15),

       ('DmJYYmL1', 'very hard', 1, 16),
       ('aKHNr356', 'very hard', 2, 17),
       ('Wav9uNyp', 'very hard', 3, 18),
       ('mX6KZSp1', 'very hard', 4, 19),
       ('bTGyf5p9', 'very hard', 5, 20),

       ('cRCGnvgT', 'kaizo easy', 1, 16),
       ('efmMIGjA', 'kaizo easy', 2, 17),
       ('kgmOOFbJ', 'kaizo easy', 3, 18),
       ('b8j0bflD', 'kaizo easy', 4, 19),
       ('ekAfXr7G', 'kaizo easy', 5, 20),

       ('IOaHYcoQ', 'kaizo medium', 1, 21),
       ('O62UtRpW', 'kaizo medium', 2, 22),
       ('ClekguIi', 'kaizo medium', 3, 23),
       ('GMToYZN9', 'kaizo medium', 4, 24),
       ('A8looKm9', 'kaizo medium', 5, 25),

       ('7wktUNQs', 'kaizo hard', 1, 26),
       ('hQBisMrt', 'kaizo hard', 2, 27),
       ('L6rK6T5u', 'kaizo hard', 3, 28),
       ('G9jd8N4J', 'kaizo hard', 4, 29),
       ('JSOpn1FJ', 'kaizo hard', 5, 30),

       ('UyOEq7Ri', 'kaizo very hard', 1, 31),
       ('m8q5IYGS', 'kaizo very hard', 2, 32),
       ('cgUvWibP', 'kaizo very hard', 3, 33),
       ('5KZATkEa', 'kaizo very hard', 4, 34),
       ('m5mMuBmk', 'kaizo very hard', 5, 35);
