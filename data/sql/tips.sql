CREATE TABLE IF NOT EXISTS `tips`
(
    `id`     int unsigned NOT NULL AUTO_INCREMENT,
    `tip_id` char(8)      NOT NULL,
    `tip`    varchar(500) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uq_tip_id` (`tip_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

INSERT IGNORE INTO `tips` (tip_id, tip)
VALUES ('AhrSd2LN', 'Whatever you make, test it before you share it!'),
       ('z1dNmBIv', 'Make it Work'),
       ('OdvnC2yD', 'Keep it simple'),
       ('oD8heMG0', 'Read more Tips!');
