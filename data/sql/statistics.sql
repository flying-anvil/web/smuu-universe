CREATE TABLE IF NOT EXISTS `statistics`
(
    `date`                 DATE         NOT NULL COMMENT 'Snapshot of statistics per day',


    `count_users`          int unsigned NOT NULL,
    `count_views`          int unsigned NOT NULL,
    `count_news`           int unsigned NOT NULL,

    `count_badges`         int unsigned NOT NULL,
    `count_badges_awarded` int unsigned NOT NULL,

    `count_tutorials`      int unsigned NOT NULL,
    `count_documentations` int unsigned NOT NULL,
    `count_guides`         int unsigned NOT NULL,

    `count_levels`         int unsigned NOT NULL,
    `count_worlds`         int unsigned NOT NULL,

    `count_graphics`       int unsigned NOT NULL,
    `count_music`          int unsigned NOT NULL,
    `count_samples`        int unsigned NOT NULL,
    `count_sounds`         int unsigned NOT NULL,
    `count_blocks`         int unsigned NOT NULL,
    `count_sprites`        int unsigned NOT NULL,
    PRIMARY KEY (`date`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
