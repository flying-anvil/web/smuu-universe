CREATE TABLE `users`
(
    `id`          int unsigned NOT NULL AUTO_INCREMENT,
    `user_id`     char(8)      NOT NULL,
    `name`        varchar(32)  NOT NULL,
    `password`    char(60)     NOT NULL,
    `date_joined` DATETIME     NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uq_id` (`user_id`),
    UNIQUE KEY `uq_user` (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
