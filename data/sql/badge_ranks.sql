CREATE TABLE IF NOT EXISTS `badge_ranks`
(
    `badge_rank_id` char(8)      NOT NULL,
    `badge_id`      char(8)      NOT NULL,
    `rank_id`       char(8)      NOT NULL,
    `name`          varchar(32)  NOT NULL,
    `description`   varchar(255) NOT NULL,
    PRIMARY KEY (`badge_rank_id`),
    Unique KEY (`badge_id`, `rank_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

INSERT IGNORE INTO `badge_ranks` (badge_rank_id, badge_id, rank_id, name, description)
VALUES ('XsmBHYpR', 'vVyfFwka', '00000000', 'Admin', 'Has all the power'),

       ('EQLGc5UW', 'X8uPA9Iq', 'v3u271kt', 'Beginner Builder', 'Created 1 level'),
       ('wNKz6Mzk', 'X8uPA9Iq', 'cOpUtZXd', 'Level Creator 2', 'Created 5 levels'),
       ('1HT4GOQ-', 'X8uPA9Iq', 'R32qJHJl', 'Level Creator 3', 'Created 25 levels'),
       ('f9zyzQ7Q', 'X8uPA9Iq', 'NtTYd5hW', 'Level Creator 4', 'Created 50 levels'),
       ('E1q7svnW', 'X8uPA9Iq', '3s8cfYPq', 'Level Creator 5', 'Created 75 levels'),
       ('CHR949GT', 'X8uPA9Iq', 'khIv34jT', 'Level Creator 6', 'Created 100 levels'),
       ('ph1eBaUP', 'X8uPA9Iq', 'l50DvTH1', 'Level Creator 7', 'Created 150 levels'),
       ('3ABa2ddm', 'X8uPA9Iq', 'zRGwPcGQ', 'Level Creator 8', 'Created 200 levels'),

       ('RWOXUNoV', '9nDKzNy1', 'v3u271kt', 'Shaper of worlds 1', 'Created 1 world'),
       ('0DOS3O6p', '9nDKzNy1', 'cOpUtZXd', 'Shaper of worlds 2', 'Created 3 worlds'),
       ('SiMDE5GL', '9nDKzNy1', 'R32qJHJl', 'Shaper of worlds 3', 'Created 5 worlds'),
       ('bkiGiFPd', '9nDKzNy1', 'NtTYd5hW', 'Shaper of worlds 4', 'Created 8 worlds'),
       ('GUjf9zlK', '9nDKzNy1', '3s8cfYPq', 'Shaper of worlds 5', 'Created 10 worlds'),
       ('lWSXg11u', '9nDKzNy1', 'khIv34jT', 'Shaper of worlds 6', 'Created 12 worlds'),
       ('82unFbNq', '9nDKzNy1', 'l50DvTH1', 'Shaper of worlds 7', 'Created 15 worlds'),
       ('sTuEdZ8C', '9nDKzNy1', 'zRGwPcGQ', 'Shaper of worlds 8', 'Created 20 worlds');
