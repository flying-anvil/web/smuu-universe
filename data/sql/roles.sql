CREATE TABLE `roles`
(
    `role_id` char(8)     NOT NULL,
    `name`    varchar(32) NOT NULL,
    PRIMARY KEY (`role_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

INSERT INTO `roles` (role_id, name)
VALUES ('xg0iRlFa', 'guest'),
       ('yZEPw8s5', 'member'),
       ('slLssBAb', 'creator'),
       ('9VAgzbfs', 'moderator'),
       ('JWl6lOFQ', 'manager'),
       ('rLX8sQ89', 'admin'),
       ('aB0jxVEm', 'troll');
