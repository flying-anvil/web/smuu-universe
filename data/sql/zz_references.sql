# <editor-fold desc="Content">
ALTER TABLE `levels`
    ADD CONSTRAINT `levels:difficulty_id` FOREIGN KEY (`difficulty_id`) REFERENCES difficulties(difficulty_id) ON DELETE RESTRICT,
    ADD CONSTRAINT `levels:user_id` FOREIGN KEY (`user_id`) REFERENCES users(user_id) ON DELETE RESTRICT,
    ADD CONSTRAINT `levels:world_id` FOREIGN KEY (`world_id`) REFERENCES worlds(world_id) ON DELETE SET NULL;

ALTER TABLE `level_records`
    ADD CONSTRAINT `level_records:level_id` FOREIGN KEY (`level_id`) REFERENCES levels(level_id) ON DELETE CASCADE,
    ADD CONSTRAINT `level_records:best_time_user_id` FOREIGN KEY (`best_time_user_id`) REFERENCES users(user_id) ON DELETE RESTRICT,
    ADD CONSTRAINT `level_records:best_deaths_user_id` FOREIGN KEY (`best_deaths_user_id`) REFERENCES users(user_id) ON DELETE RESTRICT;

ALTER TABLE `user_level_records`
    ADD CONSTRAINT `user_level_records:user_id` FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE,
    ADD CONSTRAINT `user_level_records:level_id` FOREIGN KEY (level_id) REFERENCES levels(level_id) ON DELETE CASCADE;

ALTER TABLE `user_levels_beaten`
    ADD CONSTRAINT `user_levels_beaten:user_id` FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE,
    ADD CONSTRAINT `user_levels_beaten:level_id` FOREIGN KEY (level_id) REFERENCES levels(level_id) ON DELETE CASCADE;

ALTER TABLE `user_scores`
    ADD CONSTRAINT `user_scores:user_id` FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE;

ALTER TABLE `user_world_records`
    ADD CONSTRAINT `user_world_records:user_id` FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE,
    ADD CONSTRAINT `user_world_records:world_id` FOREIGN KEY (world_id) REFERENCES worlds(world_id) ON DELETE CASCADE;

ALTER TABLE `user_worlds_beaten`
    ADD CONSTRAINT `user_worlds_beaten:user_id` FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE,
    ADD CONSTRAINT `user_worlds_beaten:world_id` FOREIGN KEY (world_id) REFERENCES worlds(world_id) ON DELETE CASCADE;

ALTER TABLE `world_records`
    ADD CONSTRAINT `world_records:world_id` FOREIGN KEY (`world_id`) REFERENCES worlds(world_id) ON DELETE CASCADE,
    ADD CONSTRAINT `world_records:best_time_user_id` FOREIGN KEY (`best_time_user_id`) REFERENCES users(user_id) ON DELETE RESTRICT,
    ADD CONSTRAINT `world_records:best_deaths_user_id` FOREIGN KEY (`best_deaths_user_id`) REFERENCES users(user_id) ON DELETE RESTRICT;
# </editor-fold>

ALTER TABLE `badge_ranks`
    ADD CONSTRAINT `badge_ranks:badge_id` FOREIGN KEY (`badge_id`) REFERENCES badges(badge_id) ON DELETE CASCADE,
    ADD CONSTRAINT `badge_ranks:rank_id` FOREIGN KEY (`rank_id`) REFERENCES ranks(rank_id) ON DELETE CASCADE;

ALTER TABLE `user_badges`
    ADD CONSTRAINT `user_badges:user_id` FOREIGN KEY (`user_id`) REFERENCES users(user_id) ON DELETE CASCADE,
    ADD CONSTRAINT `user_badges:badge_rank_id` FOREIGN KEY (`badge_rank_id`) REFERENCES badge_ranks(badge_rank_id) ON DELETE CASCADE;

ALTER TABLE `tip_tags`
    ADD CONSTRAINT `tip_tags:tip_id` FOREIGN KEY (`tip_id`) REFERENCES tips(tip_id) ON DELETE CASCADE,
    ADD CONSTRAINT `tip_tags:tip_tag_id` FOREIGN KEY (`tip_tag_id`) REFERENCES tips_tags(tip_tag_id) ON DELETE RESTRICT;



ALTER TABLE `user_auth_keys`
    ADD CONSTRAINT `user_auth_keys:user_id` FOREIGN KEY (`user_id`) REFERENCES users(user_id) ON DELETE CASCADE;

# <editor-fold desc="Permissions">
ALTER TABLE `role_permissions`
    ADD CONSTRAINT `role_permissions:permission_id` FOREIGN KEY (`permission_id`) REFERENCES permissions(permission_id) ON DELETE CASCADE,
    ADD CONSTRAINT `role_permissions:role_id` FOREIGN KEY (`role_id`) REFERENCES roles(role_id) ON DELETE CASCADE;

ALTER TABLE `user_roles`
    ADD CONSTRAINT `user_roles:user_id` FOREIGN KEY (`user_id`) REFERENCES users(user_id) ON DELETE CASCADE,
    ADD CONSTRAINT `user_roles:role_id` FOREIGN KEY (`role_id`) REFERENCES roles(role_id) ON DELETE CASCADE;
# </editor-fold>
