CREATE TABLE `worlds`
(
    `id`            int unsigned      NOT NULL AUTO_INCREMENT,
    `world_id`      char(12)          NOT NULL,
    `name`          varchar(255)      NOT NULL,
    `difficulty_id` char(8)           NOT NULL,
    `user_id`       char(8)           NOT NULL,
    `level_count`   smallint unsigned NOT NULL,
    `exit_count`    smallint unsigned NOT NULL,
    `date_created`  DATETIME          NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uq_world_id` (`world_id`),
    UNIQUE KEY `uq_name` (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
