CREATE TABLE IF NOT EXISTS `ranks`
(
    `rank_id` char(8)     NOT NULL,
    `order`   smallint    NOT NULL,
    `name`    varchar(32) NOT NULL,
    PRIMARY KEY (`rank_id`),
    UNIQUE KEY (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

INSERT INTO `ranks` (rank_id, `order`, name)
VALUES ('00000000', 0, ''),
       ('v3u271kt', 1, 'Paper'),
       ('cOpUtZXd', 2, 'Stone'),
       ('R32qJHJl', 3, 'Bronze'),
       ('NtTYd5hW', 4, 'Silber'),
       ('3s8cfYPq', 5, 'Gold'),
       ('khIv34jT', 6, 'Diamond'),
       ('l50DvTH1', 7, 'Platinum'),
       ('zRGwPcGQ', 8, 'Titanium');
