CREATE TABLE `user_scores`
(
    `user_id`                char(8)      NOT NULL,
    `score_total`            int unsigned NOT NULL,
    `score_total_casual`     int unsigned NOT NULL,
    `score_total_kaizo`      int unsigned NOT NULL,
    `score_casual_easy`      int unsigned NOT NULL,
    `score_casual_medium`    int unsigned NOT NULL,
    `score_casual_hard`      int unsigned NOT NULL,
    `score_casual_very hard` int unsigned NOT NULL,
    `score_kaizo_easy`       int unsigned NOT NULL,
    `score_kaizo_medium`     int unsigned NOT NULL,
    `score_kaizo_hard`       int unsigned NOT NULL,
    `score_kaizo_very hard`  int unsigned NOT NULL,
    `date_refreshed`         DATETIME     NOT NULL,
    PRIMARY KEY (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
