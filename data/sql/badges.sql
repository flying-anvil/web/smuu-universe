CREATE TABLE IF NOT EXISTS `badges`
(
    `badge_id`       char(8)      NOT NULL,
    `name`           varchar(32)  NOT NULL,
    `description`    varchar(255) NOT NULL,
    PRIMARY KEY (`badge_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

INSERT IGNORE INTO `badges` (badge_id, name, description)
VALUES ('vVyfFwka', 'Admin', 'Has all the power'),
       ('X8uPA9Iq', 'Level Builder', 'Create level'),
       ('9nDKzNy1', 'Shaper of worlds', 'Create worlds');
