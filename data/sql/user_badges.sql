CREATE TABLE `user_badges`
(
    `user_id`       char(8)  NOT NULL,
    `badge_rank_id` char(8)  NOT NULL,
    `date_awarded`  DATETIME NOT NULL,
    PRIMARY KEY (`user_id`, `badge_rank_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
