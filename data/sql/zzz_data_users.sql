# Only use this for testing/development

INSERT INTO `users` (user_id, name, password, date_joined) VALUES
('00000000', 'testing', '$2y$10$rTU04R6FDJJRAUOcfUH27OG3RL346uNzCdPM2Iw3cBiE.AOpl2N7i', '2021-09-02 11:43:00'); # 1234

INSERT INTO `user_roles` (user_id, role_id) VALUES
('00000000', 'yZEPw8s5'),
('00000000', 'slLssBAb'),
('00000000', '9VAgzbfs'),
('00000000', 'rLX8sQ89');

INSERT INTO user_badges (user_id, badge_id, date_awarded) VALUES
('00000000', 'vVyfFwka', NOW());

INSERT INTO user_auth_keys (`key`, name, user_id, date_created, date_expire) VALUES
('0000000000000000000000000000000000000000000000000000000000000000', 'Only for testing!', '00000000', '2021-09-03 13:22:00', '2050-01-01');
