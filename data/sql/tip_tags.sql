CREATE TABLE IF NOT EXISTS `tip_tags`
(
    `tip_id`     char(8) NOT NULL,
    `tip_tag_id` char(8) NOT NULL,
    PRIMARY KEY (`tip_id`, `tip_tag_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

INSERT IGNORE INTO `tip_tags` (tip_id, tip_tag_id)
VALUES ('AhrSd2LN', 'onDS1HIb'),
       ('AhrSd2LN', 'oFcOKEnH'),
       ('AhrSd2LN', 'z3Y4pAoZ'),
       ('AhrSd2LN', 'eEfrlip3'),
       ('z1dNmBIv', 'onDS1HIb'),
       ('OdvnC2yD', 'ifXauszy'),
       ('oD8heMG0', 'onDS1HIb');
