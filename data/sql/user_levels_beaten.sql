CREATE TABLE `user_levels_beaten`
(
    `user_id`          char(8)           NOT NULL,
    `level_id`         char(16)          NOT NULL,
    `time_spent_first` int unsigned      NOT NULL COMMENT 'seconds',
    `deaths_first`     int unsigned      NOT NULL,
    `time_spent_total` int unsigned      NOT NULL COMMENT 'seconds',
    `deaths_total`     int unsigned      NOT NULL,
    `times_beaten`     smallint unsigned NOT NULL,
    `date_beaten`      DATETIME          NOT NULL,
    PRIMARY KEY (`user_id`, `level_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
