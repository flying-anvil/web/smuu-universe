CREATE TABLE `permissions`
(
    `permission_id`    char(8)     NOT NULL,
    `name`             varchar(64) NOT NULL,
    `affects_frontend` boolean     NOT NULL DEFAULT false,
    PRIMARY KEY (`permission_id`),
    UNIQUE KEY `uq_permission` (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

INSERT IGNORE INTO `permissions` (permission_id, name, affects_frontend)
VALUES ('9QPfMt6x', 'forum.post.create', true),
       ('VJZlDVsd', 'forum.post.delete', true),
       ('YnF1bVuU', 'forum.post.hide', true),
       ('0zSZesD6', 'forum.post.show', true),
       ('FVIkK3Yq', 'forum.post.feature', true),

       ('QXOHxpSY', 'forum.tutorial.create', true),
       ('nthlolUK', 'forum.tutorial.delete', true),
       ('tBr6R53F', 'forum.tutorial.hide', true),
       ('Jglf3tsU', 'forum.tutorial.show', true),
       ('12TNAnwk', 'forum.tutorial.approve', true),
       ('q6gcsRhQ', 'forum.tutorial.feature', true),

       ('zy6y9IX7', 'content.level.delete', true),
       ('NI0RCDW2', 'content.level.hide', true),
       ('Iv51vTlY', 'content.level.show', true),
       ('0B6Ro6j2', 'content.level.approve', true),
       ('ulggwGpa', 'content.level.feature', true),

       ('WP24GmI3', 'content.world.delete', true),
       ('pYUmtGmg', 'content.world.hide', true),
       ('07BVzsis', 'content.world.show', true),
       ('zoU3Kuga', 'content.world.approve', true),
       ('RDY7Sz0y', 'content.world.feature', true),

       ('YFhtEi0t', 'user.create', true),
       ('btcsGpry', 'user.delete', true),
       ('26NdPOju', 'user.modify', true),
       ('XOUhYKBa', 'user.modify_roles', true),

       ('H5niw4dw', 'role.create', true),
       ('8VZvaP84', 'role.delete', true),
       ('eB8NJvYv', 'role.modify', true),

       ('u7XwmKbt', 'permission.create', true),
       ('EvaIYekJ', 'permission.delete', true),
       ('182YidEe', 'permission.modify', true),

       ('B8Uawdp5', 'badge.create', true),
       ('3NLWDDIn', 'badge.delete', true),
       ('mtHPcE06', 'badge.modify', true),

       ('LakVbFAg', 'tip.create', true),
       ('lr9zWWRm', 'tip.delete', true),
       ('98BOMPSb', 'tip.modify', true);
