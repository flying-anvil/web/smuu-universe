CREATE TABLE `user_world_records`
(
    `user_id`     char(8)           NOT NULL,
    `world_id`    char(12)          NOT NULL,
    `best_time`   int unsigned      NOT NULL COMMENT 'milliseconds',
    `best_deaths` smallint unsigned NOT NULL,
    PRIMARY KEY (`user_id`, `world_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
