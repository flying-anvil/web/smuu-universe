CREATE TABLE IF NOT EXISTS `tips_tags`
(
    `tip_tag_id` char(8)     NOT NULL,
    `tag_name`   varchar(32) NOT NULL,
    PRIMARY KEY (`tip_tag_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

INSERT IGNORE INTO `tips_tags` (tip_tag_id, tag_name)
VALUES ('onDS1HIb', 'General'),
       ('oFcOKEnH', 'Universe'),
       ('z3Y4pAoZ', 'Content'),
       ('eEfrlip3', 'Resource'),
       ('tfcTySLS', 'Level'),
       ('tzjueXcO', 'World'),
       ('rrv4xbpb', 'Palette'),
       ('5xh9c6LZ', 'Graphic'),
       ('X1vBohUU', 'Music'),
       ('4xZn1W8W', 'Sample'),
       ('HssltHiz', 'Block'),
       ('WFoVXGec', 'Sprite'),
       ('gEnDIebx', 'Tutorial'),
       ('sYtNxWp5', 'Guide'),
       ('ifXauszy', 'Script');
