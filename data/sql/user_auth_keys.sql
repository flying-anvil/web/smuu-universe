CREATE TABLE `user_auth_keys`
(
    `key`          char(64)    NOT NULL,
    `name`         varchar(32) NOT NULL,
    `user_id`      char(8)     NOT NULL,
    `date_created` DATETIME    NOT NULL,
    `date_expire`  DATETIME    NOT NULL,
    PRIMARY KEY (`key`),
    UNIQUE KEY `uq_name` (`user_id`, `name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
