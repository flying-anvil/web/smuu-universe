CREATE TABLE `user_level_records`
(
    `user_id`     char(8)           NOT NULL,
    `level_id`    char(8)           NOT NULL,
    `best_time`   int unsigned      NOT NULL COMMENT 'milliseconds',
    `best_deaths` smallint unsigned NOT NULL,
    PRIMARY KEY (`user_id`, `level_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
